#include "glcontext.h"

// http://doc.trolltech.com/qq/qq06-glimpsing.html

GLContext::GLContext(const QGLFormat & format, QPaintDevice * device ) 
  : QGLContext(format, device) {
}

GLContext::~GLContext() {
}

#ifdef WIN32
int GLContext::choosePixelFormat ( void * dummyPfd, HDC pdc ) {
  PIXELFORMATDESCRIPTOR *pfd = (PIXELFORMATDESCRIPTOR *)dummyPfd;
  pfd->dwFlags |= PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_TYPE_RGBA;
  int pfiMax = DescribePixelFormat(pdc, 0, 0, NULL);
  int pfi;
  for (pfi = 1; pfi <= pfiMax; pfi++) {
    DescribePixelFormat(pdc, pfi, sizeof(PIXELFORMATDESCRIPTOR), pfd);
    if (pfd->cDepthBits >= 24 && pfd->dwFlags&PFD_DRAW_TO_WINDOW &&
	pfd->dwFlags&PFD_SUPPORT_OPENGL && pfd->dwFlags&PFD_DOUBLEBUFFER /*&&
	pfd->dwFlags&PFD_TYPE_RGBA*/)
      return pfi;
  }
  pfi = QGLContext::choosePixelFormat(pfd, pdc);
  qWarning("32-bit depth unavailable: using %d bits", pfd->cDepthBits);
  return pfi;
  //   PIXELFORMATDESCRIPTOR pfd = { 
//     sizeof(PIXELFORMATDESCRIPTOR),  //  size of this pfd 
//     1,                     // version number 
//     PFD_DRAW_TO_WINDOW |   // support window 
//     PFD_SUPPORT_OPENGL |   // support OpenGL 
//     PFD_DOUBLEBUFFER,      // double buffered 
//     PFD_TYPE_RGBA,         // RGBA type 
//     24,                    // 24-bit color depth 
//     0, 0, 0, 0, 0, 0,      // color bits ignored 
//     0,                     // no alpha buffer 
//     0,                     // shift bit ignored 
//     0,                     // no accumulation buffer 
//     0, 0, 0, 0,            // accum bits ignored 
//     24,                    // 32-bit z-buffer     
//     0,                     // no stencil buffer 
//     0,                     // no auxiliary buffer 
//     PFD_MAIN_PLANE,        // main layer 
//     0,                     // reserved 
//     0, 0, 0                // layer masks ignored 
//   }; 
//   HDC  hdc;
//   int  iPixelFormat; 
//   PIXELFORMATDESCRIPTOR *pfd = (PIXELFORMATDESCRIPTOR *) dummyPfd;
//   pfd->cDepthBits = 24;

//   return(ChoosePixelFormat(hdc, dummyPfd));
}
#endif
