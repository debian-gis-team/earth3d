#ifndef FORMAGENTINPUT_H
#define FORMAGENTINPUT_H

#include <qdialog.h>

class FormAgentInput : public QDialog {
 public:
  FormAgentInput(QString xmlformular);
  ~FormAgentInput();

  QString getXMLResult();
};

#endif
