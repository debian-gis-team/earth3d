#include "geometryTranslateWrapper.h"

GeometryTranslateWrapper::GeometryTranslateWrapper(Geometry2D3D *original, Point3D translation, float scale) {
  this->original = original;
  this->translation = translation;
  this->scale = scale;
}

Point3D GeometryTranslateWrapper::getPoint(Point2D p, float factor=1.f) {
  return(original->getPoint(p, factor)-translation/scale);
}

char *GeometryTranslateWrapper::getType() {
  return(original->getType());
}

Point2D GeometryTranslateWrapper::inverse(Point3D p) {
  return(original->inverse((p*scale)+translation));
}

