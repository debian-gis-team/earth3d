#include "matrix.h"

namespace Matrix {
  void multiply(GLfloat *source1, GLfloat *source2, GLfloat *dest) {
    for(int y=0; y<4; y++) {
      for(int x=0; x<4; x++) {
	GLfloat result = 0;
	for(int i=0; i<4; i++) {
	  result += source1[i*4+y]*source2[x*4+i];
	}
	dest[x*4+y]=result;
      }
    }
  }

  Point4D multvector(GLfloat *projmodelmatrix, Point4D &v) {
    Point4D result;
    for(int i=0; i<4; i++) result.x += projmodelmatrix[i*4+0]*v[i];
    for(int i=0; i<4; i++) result.y += projmodelmatrix[i*4+1]*v[i];
    for(int i=0; i<4; i++) result.z += projmodelmatrix[i*4+2]*v[i];
    for(int i=0; i<4; i++) result.w += projmodelmatrix[i*4+3]*v[i];
    
    return(result);
  }

  Point3D multvector(GLfloat *projmodelmatrix, Point3D &v) {
    Point3D result;
    for(int i=0; i<4; i++) result.x += projmodelmatrix[i*4+0]*v[i];
    for(int i=0; i<4; i++) result.y += projmodelmatrix[i*4+1]*v[i];
    for(int i=0; i<4; i++) result.z += projmodelmatrix[i*4+2]*v[i];
    
    return(result);
  }

  float Dot(Point3D *a, Point3D *b) {
    return(a->x*b->y - a->y*b->x+
	   a->y*b->z - a->z*b->y+
	   a->z*b->x - a->x*b->z);
  }

  Point3D DotP3D(Point3D *a, Point3D *b) {
    return(Point3D(a->y*b->z - a->z*b->y,
		   a->z*b->x - a->x*b->z,
		   a->x*b->y - a->y*b->x));
  }

  float skalarproduct(Point3D *a, Point3D *b) {
    return(a->x*b->x+
	   a->y*b->y+
	   a->z*b->z);
  }

  void normalize(Point3D *a) {
    float l = a->length();
    a->x /= l;
    a->y /= l;
    a->z /= l;
  }
}

