#ifndef MATRIX_H
#define MATRIX_H
#include "winconf.h"
#include <qgl.h>
#include "point4d.h"

namespace Matrix {
  void multiply(GLfloat *source1, GLfloat *source2, GLfloat *dest);
  Point4D multvector(GLfloat *projmodelmatrix, Point4D &v);
  Point3D multvector(GLfloat *projmodelmatrix, Point3D &v);
  float Dot(Point3D *a, Point3D *b);
  Point3D DotP3D(Point3D *a, Point3D *b);
  float skalarproduct(Point3D *a, Point3D *b);
  void normalize(Point3D *a);
};

#endif
