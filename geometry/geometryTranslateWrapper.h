#ifndef GEOMETRYTRANSLATEWRAPPER_H
#define GEOMETRYTRANSLATEWRAPPER_H
#include "geometry2d3d.h"

class GeometryTranslateWrapper : public Geometry2D3D {
  Geometry2D3D *original;
  float scale;
  Point3D translation;

 public:
  GeometryTranslateWrapper(Geometry2D3D *original, Point3D translation, float scale);

  virtual Point3D getPoint(Point2D p, float factor=1.f);
  virtual char *getType();
  virtual Point2D inverse(Point3D p);
};

#endif
