#include "geometry2d3dplane.h"
#include "geometry2d3dFactory.h"

Geometry2D3DPlane geoPlane;

Geometry2D3DPlane::Geometry2D3DPlane() {
  Geometry2D3DFactory::getFactory()->registerGeometry(this);
}

Geometry2D3DPlane::~Geometry2D3DPlane() {
}

char *Geometry2D3DPlane::getType() {
  return("plane");
}

Point3D Geometry2D3DPlane::getPoint(Point2D p, float factor) {
  return(Point3D(p.x, factor-1.f, p.y));
}

/** This function does the inverse operation to getPoint. */
Point2D Geometry2D3DPlane::inverse(Point3D p) {
  return(Point2D(p.x, p.z));
}

