#ifndef GEOMETRY2D3DSPHERE_H
#define GEOMETRY2D3DSPHERE_H
#include "geometry2d3d.h"
#include "globalsettings.h"

#define SINTABLESIZE 10000

class Geometry2D3DSphere : public Geometry2D3D {
  DOUBLE *sintable;

  inline DOUBLE getSin(DOUBLE x);

 public:
  Geometry2D3DSphere();
  virtual ~Geometry2D3DSphere();

  virtual char *getType();

  virtual Point3D getPoint(Point2D p, float factor=1.f);
  /** This function does the inverse operation to getPoint. */
  virtual Point2D inverse(Point3D p);
};

#endif
