#ifndef RECT2D_H
#define RECT2D_H
#include "point2d.h"

class Rect2D {
 public:
  Point2D p, size;

  Rect2D() {
    p = Point2D(0,0);
    size = Point2D(0,0);
  };

  Rect2D(Point2D p, Point2D size) {
    this->p = p;
    this->size = size;
  };
};

#endif
