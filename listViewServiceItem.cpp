#include "listViewServiceItem.h"
#include <qdom.h>
#include <qpixmap.h>

ListViewServiceItem::ListViewServiceItem ( QListViewItem *parent, QListViewItem *after, QDomNode text, const char *connection, const char *sender, int countParts, struct part_t *parts, ConnectNetworkService *cns, QString form )
  : QListViewItem(parent, after) {
  const char *prop = text.toElement().attribute("name");
  setText(0, prop);
  this->cns = cns;
  this->form = form;

  /* find icon/map element */
  QDomNode iconnode = text.firstChild();
  while(!iconnode.isNull()) {
    if (iconnode.isElement() && (iconnode.toElement().tagName() == QString("icon") || iconnode.toElement().tagName() == QString("map"))) {
      break;
    }

    iconnode = iconnode.nextSibling();
  }

  if (!iconnode.isNull()) {
    const char *attachment = iconnode.toElement().attribute("attachment");
    if (attachment) {
      QPixmap pixmap;
      int attachmentnr = atoi(attachment);
#ifdef EARTH3DDEBUG      
      printf("Attachmentnr: %i\n", attachmentnr);
#endif
      pixmap.loadFromData((const unsigned char *) parts[attachmentnr].content, (unsigned int) parts[attachmentnr].size);
      pixmap.detach();
      setPixmap(0, pixmap);
    }
  }

  if (connection!=NULL) {
    this->connection = new char[strlen(connection)+1];
    strcpy(this->connection, connection);
  }
  else {
    this->connection = NULL;
  }

  if (sender!=NULL) {
    this->sender = new char[strlen(sender)+1];
    strcpy(this->sender, sender);
  }
  else {
    this->sender = NULL;
  }
}

ListViewServiceItem::ListViewServiceItem ( QListView *parent, QString text )
  : QListViewItem(parent) {
  setText(0, text);

  this->connection = NULL;
  this->sender = NULL;
  this->form = "";
}

ListViewServiceItem::ListViewServiceItem ( QListViewItem *parent, QListViewItem *after, QString text, QPixmap iconpixmap )
  : QListViewItem(parent, after) {
  setText(0, text);

  this->connection = NULL;
  this->sender = NULL;
  this->form = "";
  this->iconpixmap = iconpixmap;

  if (!this->iconpixmap.isNull()) {
    this->iconpixmap.detach();
    setPixmap(0, this->iconpixmap);
  }
}

ListViewServiceItem::~ListViewServiceItem () {
  if (connection) delete[](connection);
  if (sender) delete[](sender);
}

char *ListViewServiceItem::getConnection() {
  return(connection);
}

char *ListViewServiceItem::getSender() {
  return(sender);
}

ConnectNetworkService *ListViewServiceItem::getConnectNetworkService() {
  return(cns);
}

QString ListViewServiceItem::getForm() {
  return(form);
}
