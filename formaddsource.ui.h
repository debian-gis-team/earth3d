/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

void FormAddSource::init() {
  qfiledialog.addFilter("special XML files (*.sxml)");
}

void FormAddSource::setFormView( FormView *formview )
{
  this->formview = formview;
}

void FormAddSource::addSlot()
{
  if (radioButtonMarketplace->isChecked()) {
    formview->addMarketplaceSlot(lineEditMarketplaceHost->text(), lineEditMarketplacePort->text().toInt());
    hide();
  }
  if (radioButtonURL->isChecked()) {
    formview->addSource("<url address=\""+lineEditURL->text()+"\" />");
    hide();
  }
}

void FormAddSource::radioMarketplaceSlot( int status )
{
  if (status==QButton::On) {
    lineEditMarketplaceHost->setEnabled(TRUE);
    lineEditMarketplacePort->setEnabled(TRUE);
    lineEditURL->setEnabled(FALSE);
    pushButtonBrowse->setEnabled(FALSE);
  }
  else {
    lineEditMarketplaceHost->setEnabled(FALSE);
    lineEditMarketplacePort->setEnabled(FALSE);
    lineEditURL->setEnabled(TRUE);
    pushButtonBrowse->setEnabled(TRUE);
  }
}

void FormAddSource::browseFilesSlot()
{
  qfiledialog.setMode(QFileDialog::ExistingFile);
  int result = qfiledialog.exec();
  if (result==QDialog::Accepted) {
    QString selectedFile = qfiledialog.selectedFile();

    lineEditURL->setText(QString("file:///"+selectedFile));
  }
}
