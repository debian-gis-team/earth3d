/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/


void FormOptions::setHeightfieldMultiplier( float factor )
{
    lineEditHeightfieldMultiply->setText(QString().setNum(factor));
}


void FormOptions::setConcurrentDownloads( int downloads )
{
/*     lineEditDownloads->setText(QString().setNum(downloads)); */
}


void FormOptions::setEarthView( int earthview )
{
    switch(earthview) {
    case 0:
	radioButtonEarthNormal->setChecked(TRUE);
	break;
    case 1:
	radioButtonEarthGrid->setChecked(TRUE);
	break;
    case 2:
	radioButtonEarthBorder->setChecked(TRUE);
	break;
    }
}

float FormOptions::getHeightfieldMultiplier()
{
    return(lineEditHeightfieldMultiply->text().toFloat());
}


int FormOptions::getConcurrentDownloads()
{
/*     return(lineEditDownloads->text().toInt()); */
  return(0);
}


int FormOptions::getEarthView()
{
    if (radioButtonEarthNormal->isChecked()) {
	return(0);
    }
    if (radioButtonEarthGrid->isChecked()) {
	return(1);
    }
    if (radioButtonEarthBorder->isChecked()) {
	return(2);
    }
    return(0);
}

void FormOptions::setProxy( bool proxyused, QString proxyname, int proxyport ) {
  checkBoxProxy->setChecked(proxyused);
  lineEditProxyname->setText(proxyname);
  lineEditProxyport->setText(QString::number(proxyport));
}

bool FormOptions::getProxyUsed() {
  return(checkBoxProxy->isChecked());
}

QString FormOptions::getProxyName() {
  return(lineEditProxyname->text());
}

int FormOptions::getProxyPort() {
  return(lineEditProxyport->text().toInt());
}

void FormOptions::setUseTextureCompression( bool useTextureCompression ) {
  checkBoxUseTextureCompression->setChecked(useTextureCompression);
}

bool FormOptions::getUseTextureCompression() {
  return(checkBoxUseTextureCompression->isChecked());
}

void FormOptions::setUseMultitexturing( bool useMultitexturing ) {
  checkBoxUseMultitexturing->setChecked(useMultitexturing);
}

bool FormOptions::getUseMultitexturing() {
  return(checkBoxUseMultitexturing->isChecked());
}

void FormOptions::setUseLocalCaching( bool useLocalCaching ) {
  checkBoxUseLocalCache->setChecked(useLocalCaching);
}

bool FormOptions::getUseLocalCaching() {
  return(checkBoxUseLocalCache->isChecked());
}

void FormOptions::setRotationType( int type ) {
  switch(type) {
  case 0:
    radioButtonFrontPoint->setChecked(true);
    break;
  case 1:
    radioButtonAroundCamera->setChecked(true);
    break;
  default:
    radioButtonFrontPoint->setChecked(true);
  }
}

int FormOptions::getRotationType() {
  if (radioButtonFrontPoint->isChecked()) {
    return 0;
  }
  if (radioButtonAroundCamera->isChecked()) {
    return 1;
  }

  return 0;
}

void FormOptions::setLocalCacheSize( int size_mb ) {
  lineEditCacheSizeMB->setText( QString::number(size_mb) );
}

int FormOptions::getLocalCacheSize() {
  return(lineEditCacheSizeMB->text().toInt());
}

void FormOptions::setLocalCacheLocation( QString localCacheLocation ) {
  lineEditCacheLocation->setText(localCacheLocation);
}

QString FormOptions::getLocalCacheLocation() {
  return(lineEditCacheLocation->text());
}

void FormOptions::on_pushButtonBrowseCacheLocation_clicked() {
  QString directory = QFileDialog::getExistingDirectory(getLocalCacheLocation(),
							this,
							NULL,
							"Choose a directory"
							);

  if (!directory.isNull()) {
    setLocalCacheLocation(directory);
  }
}

void FormOptions::setLocalCacheUsage( float used_mb, float size_mb ) {
  labelUsageCacheMB->setText(QString::number(double(used_mb), 'g', 2)+" MB");

  if (used_mb>size_mb) used_mb = size_mb;
  if (size_mb<=0) size_mb=0.1;

  progressBarCache->setTotalSteps(100);
  progressBarCache->setProgress((used_mb*100)/size_mb);
}
