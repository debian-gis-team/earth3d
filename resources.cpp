#include "resources.h"
#include <qdragobject.h>

QPixmap getPixmap(const char *name) {
  const QMimeSource* mime_source = QMimeSourceFactory::defaultFactory()->data( name );

  if ( mime_source == 0 ) return QPixmap();

  QPixmap pixmap;
  QImageDrag::decode( mime_source, pixmap );
  return pixmap;
}
