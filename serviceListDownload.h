#ifndef SERVICELISTDOWNLOAD_H
#define SERVICELISTDOWNLOAD_H
#include "dataReceivedListener.h"
#include "urlRawDownload.h"
#include "formview.h"

class ServiceListDownload : public DataReceivedListener {
  URLRawDownload *download;
  FormView *formview;

 public:
  ServiceListDownload(QString url, FormView *formview);
  ~ServiceListDownload();

  virtual void dataReceived(const char *response, int size, const char *sender, int countParts, struct part_t *parts, void *userdata, ConnectNetworkService *cns, StopableDownload *download);
};

#endif
