#ifndef NAVIGATOR_H
#define NAVIGATOR_H
#include "point3d.h"
#include "geometry2d3dSphere.h"
#include <qdatetime.h>

class Navigator {
  Point3D sourceMark, sourceDir, sourceUp;
  Point3D targetMark, targetDir, targetUp;
  bool autoNavigation;
  float autoNavPos;
  float autoNavDistance;
  float autoNavStep;
  float savedMAXTILESIZE;
  float savedCENTERWEIGHT;
  QTime autoNavTimeLastFrame;

  Geometry2D3DSphere gSphere;

 public:
  Point3D viewer;
  float speed;
  Point3D direction;
  Point3D up;

  Navigator();
  ~Navigator();
  void accelerate(float factor);
  void stop();
  void setViewer(Point3D newViewer);
  Point3D getViewer();
  void setDirection(Point3D newDirection);
  Point3D getDirection();
  Point3D getEarthIntersection(Point3D direction, Point3D viewer);

  Point3D getEarthIsect();
  bool rotateOnPointV(float angle);
  bool rotateOnPointH(float angle);

  void setUp(Point3D newUp);
  Point3D getUp();
  void rotate(float angle);
  void rotateLeftRight(float angle);
  void rotateOnSurface(float angle);
  void elevate(float angle);
  void elevateUp(float factor);
  void forward(float factor);
  void strafe(float factor);
  void forwardOnSurface(float factor);
  void strafeOnSurface(float factor);
  void strafeUpDown(float factor);

  /** Go one timestep or more by adding speed 
   *  to the viewer coordinates. Since speed does not neccessarily
   *  goes in the same direction as direction, you have a certain
   *  amount of simulated inertia.
   */
  void step(float steps=0.1f);

  void moveToPosition(Point3D mark, Point3D dir, Point3D up, bool lowerquality=true);
  void stopAutoNavigation();
};

#endif
