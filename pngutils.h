#ifndef PNGUTILS_H
#define PNGUTILS_H

#include <png.h>

namespace PNGUtils {
  class IOBuffer {
  public:
    char *buffer;
    int size;
    int pos;

    IOBuffer(char *newbuffer, int newsize) 
      : buffer(newbuffer), size(newsize) {
      pos = 0;
    };
  };

  /** Read a PNG file from the source buffer to the destination buffer */
  void readPNG(char *source, int sourcesize, char *destination, int destinationsize);

  /** Write a PNG file. It uses the image from source, width, height, depth
   *  to write the image into the buffer destination. The size of the PNG image
   *  is returned in destinationsize. In the beginning, destinationsize must
   *  be the size of the allocated buffer in destination.
   */
  void writePNG(char *source, int width, int height, int depth, char *destination, int *destinationsize);

  void user_read_data(png_structp png_ptr,
		      png_bytep data, png_size_t length);
  void user_write_data(png_structp png_ptr,
		       png_bytep data, png_size_t length);
  void user_flush_data(png_structp png_ptr);
};

#endif
