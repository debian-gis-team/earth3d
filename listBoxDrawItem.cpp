#include "listBoxDrawItem.h"

ListBoxDrawItem::ListBoxDrawItem(QListBox *qlistbox, Draw *draw)
  : QListBoxText(qlistbox, draw->getName()) {
  this->draw = draw;
}

ListBoxDrawItem::~ListBoxDrawItem() {
}

Draw *ListBoxDrawItem::getDraw() {
  return(draw);
}
