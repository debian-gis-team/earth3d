#ifndef GRAPHICSOBJECTSCONTAINER_H
#define GRAPHICSOBJECTSCONTAINER_H
#include <vector>
#include "draw.h"
#include <qmutex.h>
#include <qmap.h>

using namespace std;

class FormView;

class GraphicsObjectsContainer {
  vector<Draw *> objectList;
  QMutex objectListMutex;
  FormView *formview;

  /** Let the objects find identical objects by using identifiers like "planet"
   */
  QMap<QString, Draw *> gocTypes;

 public:
  GraphicsObjectsContainer(FormView *formview);
  ~GraphicsObjectsContainer();

  void add(Draw *td, QString *identifier=NULL);
  void remove(Draw *td);
  vector<Draw *> *getList();
  Draw *getListElement(int nr);
  Draw *findListElementByIdentifier(QString identifier);
  int getListSize();
};

#endif

