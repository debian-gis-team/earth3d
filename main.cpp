#include <qapplication.h>
#include <qstatusbar.h>
#include "formview.h"
#include "listBoxServiceItem.h"
#include <qnetwork.h>
#include "globalsettings.h"
#include <iostream>
#include <qdir.h>

#ifdef LINUX
#include "X11/Xlib.h"
#endif

using namespace std;

int main( int argc, char ** argv )
{
#ifdef LINUX
  XInitThreads(); // see http://doc.trolltech.com/qq/qq06-glimpsing.html
#endif

#ifdef MYMACOS
  qApp->addLibraryPath(qApp->applicationDirPath() + "/../PlugIns");
#endif

  /* http, ftp, ... */
  qInitNetworkProtocols();

  /* qt app */
  QApplication a( argc, argv );
  qApp = &a;

  readXMLConfig(a.applicationDirPath());

#ifdef LINUX
  // try to read the configuration file earth3d.xml from the current directory and
  // from the ~/.earth3d/ directory. Save it to the latter.

  readXMLConfig(QDir::homeDirPath()+QDir::separator()+QString(".earth3d")+QDir::separator());
#endif

  loadCachedAttributes();

  // init cache
  if (getAttribute("usecache", "true")=="true") {
    fileCache.setCacheLocation(getAttribute("cachelocation", QDir::homeDirPath()+QDir::separator()+QString(".earth3d")+QDir::separator()+QString("cache")));
  }

  FormView w;
  if (argc>1) {
    w.setServiceListLocation(argv[1]);
  }
  w.show();
  a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );
  w.statusBar()->message("Program started, Version 1.0.5.");
  
  return a.exec();
}
