#include "serviceListDownload.h"
#include <qdom.h>

ServiceListDownload::ServiceListDownload(QString url, FormView *formview) {
  this->formview = formview;

  download = new URLRawDownload(url.latin1(), this, NULL, false);
  download->run();
}

ServiceListDownload::~ServiceListDownload() {
  delete(download);
}

void ServiceListDownload::dataReceived(const char *response, int size, const char *sender, int countParts, struct part_t *parts, void *userdata, ConnectNetworkService *cns, StopableDownload *download) {
#ifdef EARTH3DDEBUG
  printf("serviceListDownload\n");
#endif
  // parse XML document
  QDomDocument doc;
#ifdef EARTH3DDEBUG
  printf("response: %s\n", response);
#endif
  if (doc.setContent(QCString(response, size))) {
    /* search for response tag */
    QDomNode n = doc.documentElement();
    if (!n.isNull() && n.isElement() && n.toElement().tagName() == QString("addresslist")) {
#ifdef EARTH3DDEBUG
      printf("found addresslist\n");
#endif
      QDomNode n2 = n.firstChild();
      while( !n2.isNull() ) {
	if (n2.isElement() && n2.toElement().tagName() == QString("marketplace")) {
#ifdef EARTH3DDEBUG
	  printf("added marketplace\n");
#endif
	  formview->addMarketplaceSlot(n2.toElement().attribute("host",""), n2.toElement().attribute("port","0").toInt());
	}
	if (n2.isElement() && n2.toElement().tagName() == QString("url")) {
	  QString source = QString("<url address=\"")+n2.toElement().attribute("address","")+QString("\" />");
	  formview->addSource(source);
	}
	n2 = n2.nextSibling();
      }
    }
  }
}

