#ifndef RESOURCES_H
#define RESOURCES_H
#include <qpixmap.h>
#include <qmime.h>

QPixmap getPixmap(const char *name);

#endif
