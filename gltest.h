#ifndef GLTEST_H
#define GLTEST_H

#ifdef WIN32
#include "wingl.h"
#else
#define GL_GLEXT_PROTOTYPES
#endif

#include "winconf.h"
#include <qgl.h>
#include <qevent.h>
#include <vector>
#include <qthread.h>
#include <qwaitcondition.h>
#include <qmutex.h>
#include <qdatetime.h>
#include <qpopupmenu.h>
#include <qvaluevector.h>
#include "draw.h"
#include "graphicsObjectsContainer.h"
#include "point3d.h"
#include "point2d.h"
#include "navigator.h"
#include "listViewServiceItem.h"

#ifdef LINUX
#ifdef JOYSTICK
#include <jsw.h>	/* libjsw */
#endif
#endif

class FormView;

class GLTestWidget : public QGLWidget, public QThread {
    Q_OBJECT

#ifdef WIN32
  bool mousepressed;
#endif

#ifdef LINUX
#ifdef JOYSTICK
  /* Joystick data structure. */
  js_data_struct jsd;
  bool joystick_usable;
#endif
#endif

  float rtri, rtry;
  float aspect;
  int mx, my;
  float pitch, roll;
  bool viewCulling;
  GraphicsObjectsContainer *goc;
  Navigator nav;

  QWaitCondition rendered;
  QMutex rendering, rendering2, navMutex;

  bool threadStarted;

  bool drawFinished;

  bool navRotate;

  bool old_valid, matrix_valid;

  double old_sdx, old_sdy, old_sdz;
  double   proj[16];	// This will hold our projection matrix
  double   modl[16];	// This will hold our modelview matrix
  GLint viewport[4];

  QWidget *oldparent;
  WFlags oldflags;
  QPoint oldpos;
  bool fullscreen;

  /* get depth for mouse rotation */
  int depthWidth, depthHeight;
  float *depthbuffer;
  float clippingPlanes[2];
  bool grabDepthBuffer;

  /* pre generate fonts for windows */
  virtual void generateFonts();

  QValueVector<ListViewServiceItem *> agentPopupList;

 protected:
  virtual void initializeGL();

  void render();

  QTime navtime_ctime;
  QTime navtime_nowtime;

  void init();

  QPoint contextPos;

  Geometry2D3DSphere sphere;

  /* get latitude/longitude coordinates by mouse click coordinates */
  Point2D getWorldCoordinates(int mx, int my);

  /* get viewer lat/long coordinates */
  Point2D getCurrentCoordinates();

  FormView *formview;

#ifdef LINUX
#ifdef JOYSTICK
  /* get values from joystick and move view */
  void checkJoystick();
#endif
#endif
  
public slots:
  virtual void navtimer();
  virtual void screenshotSlot(QString filename="screenshot.png");
  virtual void viewTextureSlot();
  virtual void gpsCoordinatesSlot();
  virtual void showMarkerSlot();
  virtual void agentPopupSlot(int agent);

signals:
  virtual void frameFinished();

 public:
  GLTestWidget ( QWidget * parent = 0, const char * name = 0, const GLTestWidget * shareWidget = 0, WFlags f = 0 );
  GLTestWidget ( const QGLFormat & format, QWidget * parent = 0, const char * name = 0, const GLTestWidget * shareWidget = 0, WFlags f = 0 );
  GLTestWidget ( QGLContext * context, QWidget * parent, const char * name = 0, const QGLWidget * shareWidget = 0, WFlags f = 0 );
  ~GLTestWidget ();

  virtual void paintGL();
  virtual void resizeGL(int width, int height);
  virtual void mouseMoveEvent(QMouseEvent * e);
  virtual void mousePressEvent ( QMouseEvent * e );
  virtual void mouseReleaseEvent ( QMouseEvent * e );
  virtual void mouseDoubleClickEvent (QMouseEvent *e);
  virtual void wheelEvent ( QWheelEvent * e );
  virtual void setGraphicsObjectsContainer(GraphicsObjectsContainer *goc);
  virtual void setViewer(Point3D p);
  virtual void setViewCulling(bool state);

  virtual void keyPressEvent (QKeyEvent * e);
  virtual void keyReleaseEvent (QKeyEvent * e);
  virtual void setNavigation(bool rotate);
  virtual void contextMenuEvent ( QContextMenuEvent * e );
  virtual void setFormView(FormView *formview);
  virtual void run();
  Navigator *getNavigator();

  virtual void moveToPosition(Point3D mark, Point3D dir, Point3D up, bool lowerquality=true);
  void updateScreen();

  void setFullscreen(bool fullscreen);

  virtual void addAgentPopupEntry(QString text, ListViewServiceItem *item);
  virtual void agentPopupClicked( ListViewServiceItem *item, Point2D worldcoord);
};

#endif
