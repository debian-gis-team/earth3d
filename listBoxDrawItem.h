#ifndef LISTBOXDRAWITEM_H
#define LISTBOXDRAWITEM_H
#include <qlistbox.h>
#include "draw.h"

class ListBoxDrawItem : public QListBoxText {
  Draw *draw;

 public:
  ListBoxDrawItem(QListBox *qlistbox, Draw *draw);
  ~ListBoxDrawItem();

  Draw *getDraw();
};

#endif
