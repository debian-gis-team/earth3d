#ifndef CONNECTNETWORKSERVICE_H
#define CONNECTNETWORKSERVICE_H
#include <vector>
#include <string>
#include <map>
#include "serviceFoundListener.h"
#include "dataReceivedListener.h"
#include "qServiceLib.h"
#include <qthread.h>
#include "urlDownload.h"
#include "connectNetworkServiceRequestQueue.h"
#include <qdatetime.h>
#include <qvaluevector.h>

class QueuedMessage {
 public:
  QString receiver;
  QString message;

  QueuedMessage() {
  };

  QueuedMessage(QString receiver, QString message) {
    this->receiver = receiver;
    this->message = message;
  };

  QueuedMessage(const QueuedMessage &m) {
    this->receiver = m.receiver;
    this->message = m.message;
  };
};

/** This class connects the client to the agent system and can also retrieve
 *  data by using some other protocols like http.
 */
class ConnectNetworkService : public QObject, public QThread, public ConnectNetworkServiceRequestQueue {
  Q_OBJECT

  /** Vector to save the methods for addServiceFoundListener */
  std::vector<ServiceFoundListener *> sflList;
  /** Vector to save the methods for addServiceFoundListener */
  std::vector<DataReceivedListener *> drlList;
  /** Vector to save the methods for addServiceFoundListener */
  QValueVector<QueuedMessage> preConnectMessages;

  /** The connection to the marketplace. */
  ServiceLib sl;

  /** The space where the parts from the server are saved. */
  struct part_t parts[100];
  /** The sender of the last message. */
  char sender[255];

  /** Parse and answer the received document */
  void parseDocument(int countParts, struct part_t *parts, char *sender);

  QTime requesttime;

  /** Chooses the best of all available connections and returns it. */
  char *chooseConnection(const char *xmlconnections);

  /** Requests a data object by using an URL */
  void getURL(const char *xmlconnection, long currentMessageID, bool allowCache);

  /** Receive URL data */
/*   virtual void dataReceived(const char *response, int size, const char *sender, int countParts, struct part_t *parts, void *userdata); */

 protected:
  /** Starts the request of request.
   *  @returns true if the request has been completed immediately (agent requests) or false, if it is delayed 
   */
  virtual bool requestData(ConnectNetworkServiceRequestQueueObject *request);

 public slots:
  virtual void marketplaceConnected();
  
 public:
  ConnectNetworkService(const char *host, int port);
  virtual ~ConnectNetworkService();

  /** Wait and process responses for the given time. */
  virtual void run();

  /** Send a simple xml message without attachments */
  void send(const char *receiver, const char *message);

  /** Request some data 
   * @param timeout in seconds
   */
  void getOne(const char *xmlconnections, DataReceivedListener *drl, void *userdata=NULL, int timeout=10);

  /** @returns the messageid that will be in the corresponding response message */
  long get(const char *xmlconnection, DataReceivedListener *drl, void *userdata=NULL, int timeout=10, bool allowCache=true);

  /** Parses and XML connection object for an agent tag and returns the
   *  content of its agent attribute.
   */
  QString getReceiver(const char *xmlconnection);

  void forwardMessage(const char *xmlresponse, long msgid, const char *sender, int countParts, struct part_t *parts);
  
  /** Adds a method that is called each time data was received. */
  void addDataReceivedListener(DataReceivedListener *drl);
};

#endif
