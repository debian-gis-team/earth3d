#include "serviceFoundLister.h"
#include <iostream>
#include "listViewServiceItem.h"
#include <qdom.h>
#include <assert.h>
#include "formview.h"
#include "insertListViewItemEvent.h"
QMutex serviceFoundGlobalMutex;

ServiceFoundLister::ServiceFoundLister(QListViewItem *ql) {
  qlistviewitem = ql;
}

QPixmap ServiceFoundLister::parseIcon(QDomNode n, int countParts, struct part_t *parts) {
  QPixmap pixmap;

  /* find icon/map element */
  QDomNode iconnode = n.firstChild();
  while(!iconnode.isNull()) {
    if (iconnode.isElement() && (iconnode.toElement().tagName() == QString("icon") || iconnode.toElement().tagName() == QString("map"))) {
      break;
    }

    iconnode = iconnode.nextSibling();
  }

  if (!iconnode.isNull()) {
    const char *attachment = iconnode.toElement().attribute("attachment");
    if (attachment) {
      int attachmentnr = atoi(attachment);
#ifdef EARTH3DDEBUG
      printf("Attachmentnr: %i\n", attachmentnr);
#endif
      assert(attachmentnr<countParts);
      pixmap.loadFromData((const unsigned char *) parts[attachmentnr].content, (unsigned int) parts[attachmentnr].size);
      pixmap.detach();
    }
  }

  return(pixmap);
}

void ServiceFoundLister::parseNode(QDomNode cur_result, QListViewItem *qlistviewitem, const char *sender, int countParts, struct part_t *parts, ConnectNetworkService *cns) {
  while(!cur_result.isNull()) {
    QListViewItem *lastChild = qlistviewitem->firstChild();
    if (lastChild) {
      while(lastChild->nextSibling()) {
	lastChild = lastChild->nextSibling();
      }
    }
    
    if (cur_result.isElement() && cur_result.toElement().tagName() == QString("folder")) {
      // find icon
      QPixmap iconpixmap = parseIcon(cur_result, countParts, parts);
	
#ifdef WIN32
      // windows cannot create list items in this thread but we need one to place the other nodes under it, ignore folder
      parseNode(cur_result.firstChild(), qlistviewitem, sender, countParts, parts, cns);
#else
      QListViewItem *qlistviewitemNew = new ListViewServiceItem(qlistviewitem, lastChild, cur_result.toElement().attribute("name",""), iconpixmap);
      parseNode(cur_result.firstChild(), qlistviewitemNew, sender, countParts, parts, cns);
#endif

    }

    if (cur_result.isElement() && cur_result.toElement().tagName() == QString("service")) {

      QString form = "";
      char *connection = NULL;

      // search the connection node
      QDomNode connCur = cur_result.firstChild();
      while(!connCur.isNull()) {
#ifdef EARTH3DDEBUG      
	if (connCur.isElement()) {
	  std::cout << connCur.toElement().tagName() << std::endl;
	}
#endif

	if (connCur.isElement() && connCur.toElement().tagName() == QString("form")) {
	  QString output;
	  QTextStream ts(&output, IO_WriteOnly);
	  ts << connCur;
	
#ifdef EARTH3DDEBUG	
	  std::cout << "xml form content: " << std::endl << output.latin1() << std::endl;
#endif
	  form = output;
	}

	if (connCur.isElement() && connCur.toElement().tagName() == QString("connections")) {
	  QString output;
	  QTextStream ts(&output, IO_WriteOnly);
	  ts << connCur;

#ifdef EARTH3DDEBUG		
	  std::cout << "xml content: " << std::endl << output.latin1() << std::endl;
#endif
	  if (connection) delete[](connection);
	  connection = new char[output.length()+1];
	  strcpy(connection, output.latin1());
	}
		    
	connCur = connCur.nextSibling();
      }

      // 	    new ListViewServiceItem(qlistviewitem, lastChild, cur_result, connection, sender, countParts, parts, cns);
      qApp->postEvent(formview, new InsertListViewItemEvent(qlistviewitem, lastChild, cur_result, connection, sender, countParts, parts, cns, form));

      if (connection) {
	delete[](connection);
	connection = NULL;
      }
    }

    cur_result = cur_result.nextSibling();
  }
}

void ServiceFoundLister::dataReceived(const char *xmlresponse, int size, const char *sender, int countParts, struct part_t *parts, void *userdata, ConnectNetworkService *cns, StopableDownload *download) {
  if (!xmlresponse) return;

  QMutexLocker qml(&serviceFoundGlobalMutex);

//   qApp->lock();
#ifdef EARTH3DDEBUG
  std:: cout << "SERVICEFOUND " << xmlresponse << std::endl;
#endif
  /* create DOM structure */
  QDomDocument doc;
  if (doc.setContent(QCString(xmlresponse, size))) {
    QDomNode cur = doc.documentElement();
    
    while(!cur.isNull()) {
      if (cur.isElement() && cur.toElement().tagName() == QString("response")) {
	QDomNode cur_result = cur.firstChild();
	
	parseNode(cur_result, qlistviewitem, sender, countParts, parts, cns);
      }
      
      cur = cur.nextSibling();
    }

//     qlistviewitem->listView()->triggerUpdate();
  }
}

