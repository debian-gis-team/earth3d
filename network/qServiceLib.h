#ifndef QSERVICELIB_H
#define QSERVICELIB_H
#include <qsocketdevice.h>
#include <qdns.h>

struct part_t {
  int size;
  char *content;
};

class ServiceLib : public QObject {
  Q_OBJECT

  char identity[256];
  QSocketDevice socketdevice;
  QDns dns;
  int port;

  bool isConnected;

  void getline(char *line, int size, int timeout);
  int getnumber(int timeout);

  void disconnect();

 protected slots:
  void hostFound();
 signals:
  void marketplaceConnected();

 public:
  ServiceLib();
  virtual ~ServiceLib();

  void connectToServer(const char *hostaddress, int port);
  void login(int timeout);

  int readParts(char *senderID, int senderIDSize, struct part_t *parts, int maxParts, int timeout=-1);
  static struct part_t *copyParts(struct part_t *parts, int countParts);
  static void freeParts(struct part_t *parts, int countParts);
  void sendHeader(const char *receiver, int parts);
  void sendPart(int size, const void *part);
  void sendPartString(char *part);
  bool getIsConnected();
};

#endif
