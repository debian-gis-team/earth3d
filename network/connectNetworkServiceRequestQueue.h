#ifndef CONNECTNETWORKSERVICEREQUESTQUEUE
#define CONNECTNETWORKSERVICEREQUESTQUEUE
#include <map>
#include <list>
#include "connectNetworkServiceRequestQueueObject.h"
#include "urlDownload.h"
#include <qthread.h>
#include <qmutex.h>
#include <qdatetime.h>

struct TimedDataReceivedListener {
  DataReceivedListener *drl;
  QTime timeout;
  bool downloadStopped;

  /** some data the user can freely specify and that is passed to the DataReceivedListener */
  void *userdata;

  /** if a urldownload object belongs to this download, it is saved here */
  URLDownload *urldownload;

  TimedDataReceivedListener(DataReceivedListener *drl, QTime timeout, void *userdata) {
    this->drl = drl;
    this->timeout = timeout;
    this->userdata = userdata;

    this->urldownload = NULL;
    downloadStopped = false;
  };
};

extern long currentMessageID;

class ConnectNetworkServiceRequestQueue : public DataReceivedListener, public StopableDownload {
  /** Map the message IDs to their handler for forwarding responses */
  std::map<long, TimedDataReceivedListener *> mapHandler;

protected:
  QMutex mapHandlerMutex;

  /** The queue that holds the open but not yet activated requests */
  std::list<ConnectNetworkServiceRequestQueueObject *> queue;
  QMutex queueMutex;
  
  /** Dequeue and activate one request. */
  void dequeue();

  /** Counter for the currently running request processed */
  int runningRequests;
  
  /** Determines how many request may be running simultaneous */
  int maxRunningRequests;

 protected:
  /** Starts the request of request.
   *  @returns true if the request has been completed immediately (agent requests) or false, if it is delayed 
   */
  virtual bool requestData(ConnectNetworkServiceRequestQueueObject *request) = 0;
  
 public:
  /** Creates the queue and starts a thread that activates the requests */
  ConnectNetworkServiceRequestQueue(int maxRunningRequests=10);
  virtual ~ConnectNetworkServiceRequestQueue();

  /** Set the number of concurrent downloads */
  virtual void setMaxRunningRequests(int maxRunningRequest);

  void push(ConnectNetworkServiceRequestQueueObject *obj);
  virtual void dataReceived(const char *response, int size, const char *sender, int countParts, struct part_t *parts, void *userdata, ConnectNetworkService *cns, StopableDownload *download);

  /** Get the listener object for a specific ID.
   *  @id the identifier of a connection
   *  @returns the handler for this connection
   */
  TimedDataReceivedListener *getMapHandler(long id);

  /** Removes a handler from the list. Needed when a download is
   *  stopped and further incoming data needs to be discarded.
   */
  void eraseMapHandler(long id);

  virtual void stopDownload(Downloadable *downloadable);
};

#endif
