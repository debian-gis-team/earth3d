#ifndef SERVICEFOUNDPRINTER_H
#define SERVICEFOUNDPRINTER_H
#include "serviceFoundListener.h"

class ServiceFoundPrinter : public ServiceFoundListener {
 public:
  virtual void serviceFound(char *xmlresponse);
};

#endif
