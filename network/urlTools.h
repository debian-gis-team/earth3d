#ifndef URLTOOLS_H
#define URLTOOLS_H

namespace UrlTools {
  void rewriteRelativeURLs(char **part, int *partsize, char *baseurl);
};

#endif
