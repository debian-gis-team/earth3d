#ifndef DOWNLOADFINISHEDEVENT_H
#define DOWNLOADFINISHEDEVENT_H
#include <qevent.h>
#include "urlDownload.h"

class DownloadFinishedEvent : public QEvent {
  URLDownload *urlDownload;

 public:
  DownloadFinishedEvent(URLDownload *urlDownload);
  ~DownloadFinishedEvent();

  URLDownload *getURLDownload();
};

#endif
