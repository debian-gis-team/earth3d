#ifndef SERVICEFOUNDLISTENER_H
#define SERVICEFOUNDLISTENER_H

class ServiceFoundListener {
 public:
  virtual void serviceFound(char *xmlresponse, char *sender) = 0;
};

#endif
