#include "serviceFoundPrinter.h"
#include <iostream>

void ServiceFoundPrinter::serviceFound(char *xmlresponse) {
#ifdef EARTH3DDEBUG
  std::cout << "SERVICEFOUNDPRINTER" << std::endl;
  std::cout << xmlresponse << std::endl;
#endif
}

