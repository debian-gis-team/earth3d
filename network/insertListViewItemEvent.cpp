#include "insertListViewItemEvent.h"
#include "listViewServiceItem.h"
#include "qServiceLib.h"

InsertListViewItemEvent::InsertListViewItemEvent(QListViewItem *parent, QListViewItem *after, QDomNode text, const char *connection, const char *sender, int countParts, struct part_t *parts, ConnectNetworkService *cns, QString form)
  : QEvent(QEvent::User) {
  this->parent = parent;
  this->after = after;
  this->text = text;
  this->connection = QString(connection);
  this->sender = QString(sender);
  this->countParts = countParts;
  this->parts = ServiceLib::copyParts(parts, countParts);
  this->cns = cns;
  this->form = form;
}

InsertListViewItemEvent::~InsertListViewItemEvent() {
}

bool InsertListViewItemEvent::containsLatLong(QString form) {
  bool result = false;

  // parse the document
  QDomDocument doc;
  if (doc.setContent(QString::fromLatin1(form))) {
    /* search for form tag */
    QDomNode nform = doc.documentElement();
    while( !nform.isNull() ) {
      QDomElement e = nform.toElement(); // try to convert the node to an element.
      if( !e.isNull() && e.tagName() == QString("form")) {
	/* iterate through input tags */
	QDomNode ninput = nform.firstChild();
	while( !ninput.isNull() ) {
	  e = ninput.toElement(); // try to convert the node to an element.
	  if( !e.isNull() && e.tagName() == QString("input")) {
	    /* found input field */
	    QString name = e.attribute("name","");
	    if (name=="latitude" || name=="longitude") {
	      result = true;
	    }
	  }

	  ninput = ninput.nextSibling();
	}
      }
      nform = nform.nextSibling();
    }
  }

  return(result);
}

void InsertListViewItemEvent::createAndInsertItem(GLTestWidget *glmainwidget) {
  // create tree list entry
  ListViewServiceItem *item = new ListViewServiceItem(parent, after, text, connection.latin1(), sender.latin1(), countParts, parts, cns, form);

  // see if the XML-form-part contains fields named latitude or longitude
  if (containsLatLong(form)) {
    glmainwidget->addAgentPopupEntry(item->text(0), item);
  }

  // free memory
  ServiceLib::freeParts(parts, countParts);
}
