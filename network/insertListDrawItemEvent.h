#ifndef INSERTLISTDRAWITEMEVENT_H
#define INSERTLISTDRAWITEMEVENT_H
#include <qevent.h>
#include "draw.h"

class InsertListDrawItemEvent : public QEvent {
  Draw *draw;

 public:
  InsertListDrawItemEvent(Draw *draw);
  ~InsertListDrawItemEvent();

  Draw *getDraw();
};

#endif
