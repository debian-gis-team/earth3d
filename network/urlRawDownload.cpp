#include "urlRawDownload.h"

URLRawDownload::URLRawDownload(const char *url, DataReceivedListener *drl, void *userdata, bool allowCache) 
: URLDownload(url, drl, userdata, allowCache) {
}

URLRawDownload::~URLRawDownload() {
}

void URLRawDownload::finished(QNetworkOperation *qno) {
  printf("URLRawDownload finished\n");
  drl->dataReceived(content, contentsize, thisurl, 0, NULL, userdata, NULL, this);
}

