#ifndef STOPABLEDOWNLOAD_H
#define STOPABLEDOWNLOAD_H

class Downloadable;

class StopableDownload {
 protected:
  bool downloadStopped;

 public:
  StopableDownload();
  virtual ~StopableDownload();

  virtual void registerWithDownloadable(Downloadable *downloadable);
  virtual void deregisterWithDownloadable(Downloadable *downloadable);

  /** Stop the current download 
   *  @downloadable the Downloadable objects whos download should be stopped
   */
  virtual void stopDownload(Downloadable *downloadable);
};

#endif
