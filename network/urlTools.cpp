#include "urlTools.h"
#include <qdom.h>
#include <qtextstream.h>
#include <iostream>
#include <qstringlist.h>
#include <qregexp.h>

namespace UrlTools {
  void rewrite(QDomNode n, QString baseurlstring);
  QString concatURLs(QString baseurlstring, QString url);
};


/** Concatenate two strings, removing ".." from the address. */
QString UrlTools::concatURLs(QString baseurlstring, QString url) {
//   printf("concatURLs: %s\n", (baseurlstring+url).latin1());
  QStringList sl = QStringList::split(QRegExp("/"), baseurlstring+url, true);
  QString result = "";

  for(int pos=0; pos<sl.size(); pos++) {
    QStringList::iterator it = sl.at(pos);

    if ((*it)=="..") {
      sl.remove(it);
      pos--;
      it = sl.at(pos);
      sl.remove(it);
      pos--;
    }
  }
  return(sl.join("/"));
}

void UrlTools::rewrite(QDomNode n, QString baseurlstring) {
  while( !n.isNull() ) {
    QDomElement e = n.toElement(); // try to convert the node to an element.
    if( !e.isNull() ) {
      if (e.tagName() == QString("url")) {
        QString urladdress = e.attribute("address", "");
        if (urladdress.find(':')==-1 && urladdress[0]!='/') {
          e.setAttribute("address", UrlTools::concatURLs(baseurlstring, urladdress));
        }
      }
      else {
	if (e.tagName() == QString("sign")) {
	  QString urladdress = e.attribute("icon", "");
	  if (urladdress.find(':')==-1 && urladdress[0]!='/') {
	    e.setAttribute("icon", baseurlstring+urladdress);
	  }
	}
	else {
	  /* descend deeper */
	  UrlTools::rewrite(n.firstChild(), baseurlstring);
	}
      }
    }
  
    n = n.nextSibling();
  }
}

void UrlTools::rewriteRelativeURLs(char **part, int *partsize, char *baseurl) {
  /* remove everything after last slash */
  QString baseurlstring(baseurl);
  baseurlstring.remove(baseurlstring.findRev('/')+1, baseurlstring.length());
  
  /* update URLs */
  char *xmlstring = new char[*partsize+1];
  memcpy(xmlstring, *part, *partsize);
  xmlstring[*partsize]=0;

  QDomDocument *doc = new QDomDocument();
  if (doc->setContent(QString(xmlstring))) {
    /* search for url tag */
    QDomNode n = doc->documentElement();
    UrlTools::rewrite(n, baseurlstring);
  }
  
  /* write document to buffer */
  QByteArray outputxml;
  QTextStream textoutxml(outputxml, IO_WriteOnly);
  doc->save(textoutxml, 0);
  
  delete(doc);
  free(*part);
  delete[](xmlstring);
  *part = new char[outputxml.size()+1];
  memcpy(*part, outputxml.data(), outputxml.size());
  (*part)[outputxml.size()]=0;
  
  *partsize = outputxml.size();
}

