#ifndef SERVICEFOUNDLISTER_H
#define SERVICEFOUNDLISTER_H
#include "dataReceivedListener.h"
#include <qlistview.h>
#include <qmutex.h>
#include <qdom.h>
#include <qpixmap.h>

extern QMutex serviceFoundGlobalMutex;

class ServiceFoundLister : public DataReceivedListener {
  QListViewItem *qlistviewitem;

 protected:
  virtual void parseNode(QDomNode cur_result, QListViewItem *qlistviewitem, const char *sender, int countParts, struct part_t *parts, ConnectNetworkService *cns);
  QPixmap parseIcon(QDomNode n, int countParts, struct part_t *parts);

 public:
  ServiceFoundLister(QListViewItem *ql);
  virtual void dataReceived(const char *response, int size, const char *sender, int countParts, struct part_t *parts, void *userdata, ConnectNetworkService *cns, StopableDownload *download);
};

#endif
