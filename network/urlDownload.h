#ifndef URLDOWNLOAD_H
#define URLDOWNLOAD_H
#include <qthread.h>
#include <qurloperator.h>
#include "dataReceivedListener.h"
#include "stopableDownload.h"
#include "proxyUrlOperator.h"

class URLDownload : public ProxyUrlOperator, public StopableDownload {
  Q_OBJECT

 protected:
  char *content;
  int contentsize;
  DataReceivedListener *drl;
  void *userdata;
  char *thisurl;
  int retrynr;

  virtual void finished(QNetworkOperation *qno);

 public:
  URLDownload(const char *url, DataReceivedListener *drl, void *userdata, bool allowCache=true);
  virtual ~URLDownload();

  virtual void run();

 public slots:
  void finished_download(QNetworkOperation *qno);
  void data_received(const QByteArray &array, QNetworkOperation *qno);
};

#endif
