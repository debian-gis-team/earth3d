#ifndef PROXYURLOPERATOR_H
#define PROXYURLOPERATOR_H
#include <qnetworkprotocol.h>
#include <qcstring.h>
#include <qhttp.h>

class ProxyUrlOperator : public QObject {
  Q_OBJECT

  QString url;
  QHttp* http;
  int downloadid;
  bool allowCache;

 signals:
  void finished(QNetworkOperation *qn);
  void data(const QByteArray &ba, QNetworkOperation *qn);

 public slots:
   virtual void readyRead ( const QHttpResponseHeader & resp );
   virtual void requestFinished ( int id, bool error );
   virtual void stateChanged(int id);
   virtual void requestStarted(int id);
   virtual void responseHeaderReceived ( const QHttpResponseHeader & resp );
   virtual void dataSendProgress ( int done, int total );
   virtual void dataReadProgress ( int done, int total );

 public:
  ProxyUrlOperator(const char *url, bool allowCache=true);
  virtual ~ProxyUrlOperator();

  virtual const QNetworkOperation * get ( const QString & location = QString::null );
};

#endif
