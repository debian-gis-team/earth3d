#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "qServiceLib.h"
#include <assert.h>

ServiceLib::ServiceLib() {
  isConnected = false;
}

ServiceLib::~ServiceLib() {
}

void ServiceLib::connectToServer(const char *hostaddress, int port) {
  bool result;
  connect(&dns, SIGNAL(resultsReady()), this, SLOT(hostFound()));
  dns.setRecordType(QDns::A);
  dns.setLabel(hostaddress);
  this->port = port;

  printf("CONNECTTOSERVER\n");
}

void ServiceLib::getline(char *line, int size, int timeout) {
  do {
    char c;

    /* wait for data */
    bool timeouted = false;

    if (socketdevice.bytesAvailable()>0 || socketdevice.waitForMore(timeout*1000, &timeouted)>0) {
      socketdevice.readBlock(&c, 1);
//       printf("c=%c (%i)\n", c, c);
      if (c==10) {
	line[0]=0;
	break;
      }
      if (c!=13) {
	line[0]=c;
	line++;
      }
    }
    else {
      if (timeouted) {
	printf("TIMEOUT\n");
	throw (char *) "timeout";
      }
      else {
	throw("Connection closed");
      }
    }
  } while(1);
}

int ServiceLib::getnumber(int timeout) {
  char line[255];
  getline(line, 254, timeout);
  return(atoi(line));
}

void ServiceLib::disconnect() {
   socketdevice.close();
   isConnected = false;
 }

void ServiceLib::login(int timeout) {
  char line[255];
  getline(line, 254, timeout);
  if (line[0]!='1' || line[1]!=' ') {
    printf("Wrong response from server 1.\n");
    disconnect();
    return;
  }
  printf("read ready\n");
  int result = socketdevice.writeBlock("CONNECT\n", 8);
  printf("result=%i\n", result);

  getline(line, 254, timeout);
  if (line[0]!='4' || line[1]!=' ') {
    printf("Wrong response from server 4.\n");
    disconnect();
    return;
  }

  /* parse own identity */
  char *space = strchr(line+2, ' ');
  if (space) {
    space[0]=0;
    strcpy(identity, line+2);
  }
  else {
    printf("Wrong response from server 4, no identity.\n");
    disconnect();
  }    
}

int ServiceLib::readParts(char *senderID, int senderIDSize, struct part_t *parts, int maxParts, int timeout) {
  try {
    printf("GETLINE\n");
    getline(senderID, senderIDSize-1, timeout);
#ifdef DEBUG
    printf("SENDER %s\n", senderID);
#endif
    int countParts = getnumber(timeout);
#ifdef DEBUG
    printf("countParts=%i\n", countParts);
#endif
    for(int partNr=0; partNr<countParts; partNr++) {
      int partSize = getnumber(timeout);
#ifdef DEBUG
      printf("partSize=%i\n", partSize);
#endif
      assert(partSize>0);
      char *partBuffer = (char *) malloc(partSize);
      int readbytes = 0;
      while(readbytes<partSize) {
	readbytes += socketdevice.readBlock(partBuffer+readbytes, partSize-readbytes);
      }
      
      if (partNr<maxParts) {
	parts[partNr].size = partSize;
      parts[partNr].content = partBuffer;
      }
      else {
	free(partBuffer);
      }
    }

    return(countParts);
  }
  catch(char *e) {
    printf("error: %s\n", e);
  }
  return(0);
}

struct part_t *ServiceLib::copyParts(struct part_t *parts, int countParts) {
  part_t *result = new part_t[countParts];
  for(int partNr=0; partNr<countParts; partNr++) {
    result[partNr].size = parts[partNr].size;
    result[partNr].content = (char *) malloc(result[partNr].size);
    memcpy(result[partNr].content, parts[partNr].content, result[partNr].size);
  }  
  
  return(result);
}

void ServiceLib::freeParts(struct part_t *parts, int countParts) {
  for(int partNr=0; partNr<countParts; partNr++) {
    free(parts[partNr].content);
  }  
}

void ServiceLib::sendHeader(const char *receiver, int parts) {
  char line[255];

  // Receiver
  socketdevice.writeBlock(receiver, strlen(receiver));
  socketdevice.writeBlock("\n", 1);

  // Part count
  sprintf(line, "%i\n", parts);
  socketdevice.writeBlock(line, strlen(line));
}

void ServiceLib::sendPart(int size, const void *part) {
  char line[255];

  // Part size
  sprintf(line, "%i\n", size);
  socketdevice.writeBlock(line, strlen(line));

  // Part content
  printf("socketdevice.writeblock size=%i\n", size);

  socketdevice.writeBlock((const char *) part, size);
}

void ServiceLib::sendPartString(char *part) {
  sendPart(strlen(part), part);
}

void ServiceLib::hostFound(){
  printf("size=%i port=%i\n", dns.addresses().size(), port);
  if (!socketdevice.connect(dns.addresses()[0], port)) {
    printf("error connecting\n");
    return;
  }

  isConnected = true;

  emit marketplaceConnected();
}

bool ServiceLib::getIsConnected() {
  return(isConnected);
}

