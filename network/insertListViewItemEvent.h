#ifndef INSERTLISTVIEWITEMEVENT_H
#define INSERTLISTVIEWITEMEVENT_H
#include <qevent.h>
#include <qlistview.h>
#include <qdom.h>
#include "qServiceLib.h"
#include "connectNetworkService.h"
#include "gltest.h"

class InsertListViewItemEvent : public QEvent {
  QListViewItem *parent;
  QListViewItem *after;
  QDomNode text;
  QString connection;
  QString sender;
  int countParts;
  QString form;

  struct part_t *parts;
  ConnectNetworkService *cns;
/*   QListViewItem *newitem; */

  bool containsLatLong(QString form);

 public:
  InsertListViewItemEvent(QListViewItem *parent, QListViewItem *after, QDomNode text, const char *connection, const char *sender, int countParts, struct part_t *parts, ConnectNetworkService *cns, QString form);
  ~InsertListViewItemEvent();

  void createAndInsertItem(GLTestWidget *glmainwidget);
};

#endif
