#ifndef CONNECTNETWORKSERVICEREQUESTQUEUEOBJECT
#define CONNECTNETWORKSERVICEREQUESTQUEUEOBJECT
#include "dataReceivedListener.h"

/** This class represents one object in the
 *  ConnectNetworkServiceRequestQueue.
 */
class ConnectNetworkServiceRequestQueueObject {
  char *xmlconnection;
  DataReceivedListener *drl;
  void *userdata;
  int timeout;
  int messageid;
  bool allowCache;

public:
  ConnectNetworkServiceRequestQueueObject(const char *xmlconnection, DataReceivedListener *drl, void *userdata, int timeout, bool allowCache);
  ~ConnectNetworkServiceRequestQueueObject();

  int getTimeout();

  char *getxmlconnection();

  DataReceivedListener *getDataReceivedListener();

  void *getUserData();

  int getMessageID();
  void setMessageID(int messageid);

  /** Returns if this download permits caching using the local cache. The proxy cache is always allowed. */
  bool getAllowCache();
  void setAllowCache(bool allowCache);
};

#endif
