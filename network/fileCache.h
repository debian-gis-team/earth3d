#ifndef FILECACHE_H
#define FILECACHE_H
#include <qvaluelist.h>
#include <qmap.h>
#include <qthread.h>
#include <qmutex.h>

#include "md5.h"


/**
 * This is a cache that saves downloaded files in a directory up to a given size.
 * It uses a simple scheme: Every files that is requested is saved (or moved when it already
 * exists) to the top of the cache list. When the cache is full, entries from the bottom are removed.
 * The list is saved after every successful download and after 10 cache hits.
 */
class FileCache : public QThread {
 protected:
  QString cacheLocation;
  QValueList <QString> files;
  QString cacheIndexFilename;
  QMap<QString, int> filesizes;

  bool dirty;

 public:
  FileCache();

  void setCacheLocation(QString path);
  
  void loadCache();
  void saveCache();

  bool contains(QString url);
  QByteArray read(QString url);
  void addFile(QString url, QByteArray &ba);

  virtual void run();

  float getUsedSizeMB();

 protected:
  void loadCacheIndex(QString filename);
  void checkCacheSize();
  int getFileSize(QString filename);
  long getUsedSizeBytes();
  QString hexString(uint8 value);
  void checkCacheIndex();
  QString createFilename(QString url);
  void removeFile(QString filename);
  
  void mkpath(QString directory);
  void rmpath(QString filename);
};

#endif
