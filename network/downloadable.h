#ifndef DOWNLOADABLE_H
#define DOWNLOADABLE_H
#include <qmutex.h>
#include "stopableDownload.h"

class Downloadable {
 protected:
  int runningDownloads;
  QMutex downloadableMutex;
  StopableDownload **runningDownloadsArray;

 public:
  Downloadable();
  virtual ~Downloadable();

  virtual void addDownloader(StopableDownload *drl);
  virtual void removeDownloader(StopableDownload *drl);
  virtual void stopDownloads();
};

#endif
