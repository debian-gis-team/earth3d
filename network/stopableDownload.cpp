#include "stopableDownload.h"
#include "downloadable.h"

StopableDownload::StopableDownload() {
  downloadStopped = false;
}

StopableDownload::~StopableDownload() {
}

void StopableDownload::registerWithDownloadable(Downloadable *downloadable) {
  /* register with downloadable */
  downloadable->addDownloader(this);
}

void StopableDownload::deregisterWithDownloadable(Downloadable *downloadable) {
  /* deregister with downloadable */
  if (!downloadStopped) {
    downloadable->removeDownloader(this);
  }
}

void StopableDownload::stopDownload(Downloadable *downloadable) {
  downloadStopped = true;
}
