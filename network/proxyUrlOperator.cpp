#include "proxyUrlOperator.h"
#include <qurl.h>
#include <iostream>
#include "globalsettings.h"

using namespace std;

ProxyUrlOperator::ProxyUrlOperator(const char *url, bool allowCache) {
  this->allowCache = allowCache;

  http = new QHttp();
//   connect(http, SIGNAL(readyRead ( const QHttpResponseHeader & )), this, SLOT(readyRead ( const QHttpResponseHeader & )));
  connect(http, SIGNAL(requestFinished ( int, bool )), this, SLOT(requestFinished ( int, bool )));
  connect(http, SIGNAL(stateChanged(int)), this, SLOT(stateChanged ( int )));
  connect(http, SIGNAL(requestStarted(int)), this, SLOT(requestStarted ( int )));
  connect(http, SIGNAL(responseHeaderReceived ( const QHttpResponseHeader &)), this, SLOT(responseHeaderReceived ( const QHttpResponseHeader &)));
  connect(http, SIGNAL(dataSendProgress ( int , int  )), this, SLOT(dataSendProgress ( int , int  )));
  connect(http, SIGNAL(dataReadProgress ( int , int  )), this, SLOT(dataReadProgress ( int , int  )));

  this->url = QString(url);
}

ProxyUrlOperator::~ProxyUrlOperator() {
#ifdef EARTH3DDEBUG
  cout << "ProxyUrlOperator destructor" << endl;
#endif

  delete(http);
}

const QNetworkOperation * ProxyUrlOperator::get ( const QString & location ) {
  QUrl qurl = QUrl(url);

  std::cout << "get " << url.latin1() << " allowCache=" << allowCache << endl;

  if (allowCache && getAttribute("usecache", "true")=="true") {
    // try to download the file from the local cache first
    QByteArray fcba = fileCache.read(url);

    if (fcba.size()>0) {
      emit data(fcba, NULL);
      emit finished(NULL);
      return(NULL);
    }
  }

  // get port of connection

  int port = qurl.port();
  if (port==-1) port=80;

#ifdef EARTH3DDEBUG
  std::cout << "ProxyUrlOperator::get " << url.latin1() << endl;
  std::cout << "Host: " << QUrl(url).host() << endl;
#endif

  // create http header with or without proxy

  QHttpRequestHeader *reqhdr;

  bool proxy = false;
  if (isAttributeExisting("proxyused") && getAttribute("proxyused")=="true") {
    http->setHost(getAttribute("proxy"), getAttribute("proxyport").toInt());
    reqhdr = new QHttpRequestHeader("GET", qurl);
  }
  else {
    http->setHost(qurl.host(), port);
    reqhdr = new QHttpRequestHeader("GET", qurl.path());
  }

  reqhdr->setValue("Host", QUrl(url).host());
  
  // start download

  downloadid = http->request(*reqhdr);

  // delete header

  delete(reqhdr);
}

void ProxyUrlOperator::readyRead ( const QHttpResponseHeader & resp ) {
  QByteArray ba = http->readAll();
#ifdef EARTH3DDEBUG
  std::cout << "ProxyUrlOperator::get readyRead " << ba.size() << endl;
#endif
  
  emit data(ba, NULL);
}

void ProxyUrlOperator::requestFinished ( int id, bool error ) {
  QByteArray ba = http->readAll();
#ifdef EARTH3DDEBUG
  std::cout << "ProxyUrlOperator::get requestFinished " << ba.size() << endl;
  cout << "error: error=" << error << " errorString: " << http->errorString() << endl;
#endif

  if (id==downloadid) {
  	if (!error) {
      fileCache.addFile(url, ba);
  	}

    emit data(ba, NULL);
    emit finished(NULL);
  }
}

void ProxyUrlOperator::stateChanged(int id) {
#ifdef EARTH3DDEBUG
  cout << "new state: " << id << endl;
#endif
}
 
void ProxyUrlOperator::requestStarted(int id) {
#ifdef EARTH3DDEBUG
  cout << "request started: " << id << endl;
#endif
}

void ProxyUrlOperator::responseHeaderReceived ( const QHttpResponseHeader & resp ) {
#ifdef EARTH3DDEBUG
  cout << "response received: " << endl;
#endif
}

void ProxyUrlOperator::dataSendProgress ( int done, int total ) {
#ifdef EARTH3DDEBUG
  cout << "dataSendProgress: " << endl;
#endif
}

void ProxyUrlOperator::dataReadProgress ( int done, int total ) {
#ifdef EARTH3DDEBUG
  cout << "dataReadProgress: done=" << done << " total=" << total << endl;
#endif
}

