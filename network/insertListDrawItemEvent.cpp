#include "insertListDrawItemEvent.h"

InsertListDrawItemEvent::InsertListDrawItemEvent(Draw *draw)
  : QEvent((QEvent::Type) 1001) {
  this->draw = draw;
}

InsertListDrawItemEvent::~InsertListDrawItemEvent() {
}

Draw *InsertListDrawItemEvent::getDraw() {
  return(draw);
}
