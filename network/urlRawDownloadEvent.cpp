#include "urlRawDownloadEvent.h"

URLRawDownloadEvent::URLRawDownloadEvent(URLRawDownload *uRawDownload)
  : QEvent((QEvent::Type) 1002) {
  this->uRawDownload = uRawDownload;
}

URLRawDownloadEvent::~URLRawDownloadEvent() {
}

URLRawDownload *URLRawDownloadEvent::getURLRawDownload() {
  return(uRawDownload);
}
