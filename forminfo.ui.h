/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include <qgl.h>
#include "winconf.h"
#include <GL/glext.h>

void FormInfo::updateInfo()
{
   /* print number of texture units */
  GLint depthnumber=0;
  glGetIntegerv(GL_DEPTH_BITS, &depthnumber);
  
  /* print number of texture units */
  GLint texnumber=0;
  glGetIntegerv(GL_MAX_TEXTURE_UNITS_ARB, &texnumber);

  listViewInfo->clear();
  listViewInfo->insertItem(new QListViewItem(listViewInfo, "GL_DEPTH_BITS", QString().setNum(depthnumber)));  listViewInfo->insertItem(new QListViewItem(listViewInfo, "GL_MAX_TEXTURE_UNITS_ARB", QString().setNum(texnumber)));
}
