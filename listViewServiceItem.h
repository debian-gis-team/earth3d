#ifndef LISTVIEWSERVICEITEM_H
#define LISTVIEWSERVICEITEM_H
#include <qdom.h>
#include <qlistview.h>
#include "connectNetworkService.h"
#include "qServiceLib.h"
#include <qpixmap.h>

class ListViewServiceItem : public QListViewItem {
  char *connection;
  char *sender;
  ConnectNetworkService *cns;
  QString form;
  QPixmap iconpixmap;

 public:
  ListViewServiceItem ( QListViewItem *parent, QListViewItem *after, QDomNode text, const char *connection, const char *sender, int countParts, struct part_t *parts, ConnectNetworkService *cns, QString form );
  ListViewServiceItem ( QListView *parent, QString text );
  ListViewServiceItem ( QListViewItem *parent, QListViewItem *after, QString text, QPixmap iconpixmap );
  ~ListViewServiceItem ();

  char *getConnection();
  char *getSender();

  /** Returns the formular data of the service */
  QString getForm();
  ConnectNetworkService *getConnectNetworkService();
};

#endif
