/** This code was written by Dominique Gunia webmaster@earth3d.org */

/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include <iostream>

#include "connectNetworkService.h"
#include "serviceFoundLister.h"
#include <vector>
#include <string>
#include <iostream>
#include "urlDownload.h"
#include <qnetwork.h>
#include <qurloperator.h>
#include <qdatetime.h>
#include <qtimer.h>
#include "mapTileTree.h"
#include "textureTreeNodeCore.h"
#include "treeDrawSphere.h"
#include "treeDrawFactory.h"
#include "graphicsObjectsContainer.h"
#include "winconf.h"
#include <GL/glext.h>
#include "formstatus.h"
#include "statusObserverQT.h"
#include "geometry2d3dSphere.h"
#include "globalsettings.h"
#include <qstatusbar.h>
#include "listViewServiceItem.h"
#include "listBoxDrawItem.h"
#include "draw.h"
#include "glcontext.h"
#include "forminfo.h"
#include "formaddsource.h"
#include "formagentform.h"
#include "insertListViewItemEvent.h"
#include "insertListDrawItemEvent.h"
#include "urlRawDownloadEvent.h"
#include "downloadFinishedEvent.h"
#include "formoptions.h"
#include "earth.h"
#include "moveToPositionEvent.h"
#include "serviceListDownload.h"

FormStatus *fstatus;
FormInfo *forminfo;
FormAddSource *formaddsource;

void FormView::searchAction_activated()
{
/*   addMarketplaceSlot("elara.cg.cs.tu-bs.de", 5432); */
/*   addMarketplaceSlot("jupiter.schunter.etc.tu-bs.de", 1234); */
/* #ifndef WIN32 */
/*   addMarketplaceSlot("localhost", 5342); */
/* #endif */
/*   addSource("<url address=\"http://nethack.cg.cs.tu-bs.de/earth/service.sxml\" />"); */
/*   addSource("<url address=\"http://europa.cg.cs.tu-bs.de/earth/earth/service.sxml\" />"); */
/*   addSource("<url address=\"http://nethack.cg.cs.tu-bs.de/gps/gps2/datalist.sxml\" />"); */
/*   addSource("<url address=\"http://europa.cg.cs.tu-bs.de/earth/gps/datalist.sxml\" />"); */
/*   addSource("<url address=\"http://europa.cg.cs.tu-bs.de/html/earthview/www/service.sxml\" />"); */
/* #ifdef WIN32 */
/*   addSource("<url address=\"file:///E:/earth/service.sxml\" />"); */
/*   addSource("<url address=\"file:///E:/gps/datalist.sxml\" />"); */
/* #endif */

  statusBar()->message("Searching...", 2000);

#ifdef DAVE
  cns->getOne("<connections><url address=\"http://nethack.cg.cs.tu-bs.de/earth/geometry.mxml\"></url></connections>", tdf);

  gLMainWidget->setFullscreen(true);
#endif
}

void FormView::init()
{
  std::cout << "INIT" << endl;

  /* Create connection to agent system */
  cns = new ConnectNetworkService("localhost", 5123);

  /* Create listener to display found services */
  listViewSources->setSelectionMode(QListView::Single);
  listViewSources->setRootIsDecorated(true);

  drl = NULL;

  /* set formview variable */
  formview = this;

  /* set up listBoxDrawItems */
//  listBoxDrawItems->setSelectionMode(QListBox::Multi);

  /* Create central container for holding all objects */
  goc = new GraphicsObjectsContainer(this);

  GLContext *context = new GLContext(gLMainWidget->format(), gLMainWidget);
  gLMainWidget->setContext(context);
  gLMainWidget->setGraphicsObjectsContainer(goc);
  gLMainWidget->setFormView(this);

  /* Create listener to create TreeDraw objects from received geometries. */
  tdf = new TreeDrawFactory(goc, cns, gLMainWidget);
  
  /* show status window */
  fstatus = NULL; // new FormStatus();
/*   fstatus->show(); */
/*   ((StatusObserverQT) statusobserver).updateDisplay(); */

  alwaysBind = false;

  /* create info window */
  forminfo = new FormInfo();
  
  /* create add window */
  formaddsource = new FormAddSource();
  formaddsource->setFormView(this);

  /* test formagentform window */
/*   FormAgentForm *formagentform = new FormAgentForm(); */
/*   formagentform->setXMLInputs("<form><input label=\"Address:\" type=\"text\" name=\"ip\" value=\"www.heise.de\"/><input label=\"Test:\" type=\"text\" name=\"ip\" value=\"Hallo\"/></form>"); */
/* /\*   formagentform->set *\/ */
/*   formagentform->show(); */
  
  /* search after startup */
  QTimer *timer = new QTimer( this );
  connect( timer, SIGNAL(timeout()), this, SLOT(timerDoneAutoSearchSlot()) );
  timer->start( 1000, TRUE ); // 1 second single-shot timer

  /* init movie capture */
  movieframecount = 0;

  /* init global variables */
  maptiledrawtype = 0;
  heightfieldmultiplier =  4;
  concurrentdownloads = 10;
  serviceListLocation = "http://www.earth3d.org/earth3daddresses.xml";
}

void FormView::loadSlot()
{
  statusBar()->message("Loading...", 2000);

  /* get the selected entry */
  ListViewServiceItem *item = (ListViewServiceItem *) listViewSources->selectedItem();

  /* request the geometry object */
  loadFunction(item, QMap<QString, QString>());
}

void FormView::loadFunction(ListViewServiceItem *item, QMap<QString, QString> propertiesMap) {
  printf("<?xml version=\"1.0\"?>"
	 "%s", item->getConnection());

  /* get ConnectNetworkService with which this object was received */
  ConnectNetworkService *tmpcns;
  tmpcns = item->getConnectNetworkService();
  if (tmpcns==NULL) {
    tmpcns = cns;
  }

  if (item->getForm() != QString("")) {
    /* needs to display a form to enter some data */
    FormAgentForm *formagentform = new FormAgentForm();
    formagentform->setXMLInputs(item->getForm(), propertiesMap);
    formagentform->setRetrievalSettings(tmpcns, item->getConnection(), tdf);
    formagentform->show();
  }
  else {
    /* request it without further information requests */
    tmpcns->getOne(item->getConnection(), tdf);
  }
}
    
void FormView::testSlot()
{
  /* get number of geometry objects */
  printf("Number of geometry objects: %i\n", goc->getListSize());

  addMarketplaceSlot("localhost", 5000);

  /* do some time tests */
  Geometry2D3DSphere gs;
  Point2D p(0,0);
  QTime start = QTime::currentTime();
  for(int i=0; i<100000; i++) {
    gs.getPoint(p, 1.f);
  }
  QTime end = QTime::currentTime();
  printf("Time for 100000 x Geometry2D3DSphere.getPoint: %i\n", start.msecsTo(end));

  GLint number=0;
  glGetIntegerv(GL_DEPTH_BITS, &number);
  printf("Depth bits: %i\n", number);

/*   /\* test geometry inverse *\/ */
/*   Geometry2D3DSphere g; */

/*   for(int i=0; i<100; i++) { */
/*     printf("i: %i", i); */
/*     Point3D p3 = g.getPoint(Point2D(i/100., 0.8), 1); */
/*     printf(" point: %f, %f, %f", p3.x, p3.y, p3.z); */
/*     Point2D p2 = g.inverse(p3); */
/*     printf(" inverse: %f, %f\n", p2.x, p2.y); */
/*   } */
}


void FormView::exitSlot()
{
  delete(cns);
  if (drl) delete(drl);
  delete(goc);
  delete(tdf);

  ::exit(0);
}

void FormView::zoomChanged(int value)
{
  MAXTILESIZE=(16-value)/10.+0.1;
#ifdef LINUX
      gLMainWidget->update();
#else
      gLMainWidget->updateGL();
#endif

  needRedraw = true;
}


void FormView::checkChanged()
{
/*   gLMainWidget->setViewCulling(checkBoxViewCulling->isChecked()); */
}

void FormView::screenshotSlot()
{
  gLMainWidget->screenshotSlot();

  statusBar()->message("Screenshot saved.", 2000);
}


void FormView::centerweightSlot( int value )
{
  CENTERWEIGHT = value;
  needRedraw = true;
}

void FormView::navRotateSlot()
{
  flyMode = false;
}

void FormView::navFlySlot()
{
  flyMode = true;
}

void FormView::viewTextureSlot()
{
  statusBar()->message("Select texture with the mouse.", 10000);
}

void FormView::addSource( QString source )
{
  printf("added source %s\n", source.latin1());
  ListViewServiceItem *rootlistview = new ListViewServiceItem(listViewSources, source);

  // expand the item in the tree view
  rootlistview->setOpen(true);

  DataReceivedListener *drl = new ServiceFoundLister(rootlistview);
  cns->get(source.latin1(), drl);  
}

void FormView::addToDrawList(Draw *draw)
{
  new ListBoxDrawItem(listBoxDrawItems, draw);
}


void FormView::removeActiveSlot()
{
  for(int nr=0; nr<listBoxDrawItems->count(); nr++) {
    if (listBoxDrawItems->isSelected(nr)) {
      ListBoxDrawItem *item = (ListBoxDrawItem *) listBoxDrawItems->item(nr);
      Draw *draw = item->getDraw();
      tdf->remove(draw);
      goc->remove(draw);
/*       delete(item->getDraw()); */
/*       delete(item); */
      delete(draw);
      nr=0;
    }
  }
}

void FormView::timerDoneAutoSearchSlot()
{
  ServiceListDownload *download = new ServiceListDownload(serviceListLocation, this);
  //  searchAction_activated(); 
}


void FormView::addMarketplaceSlot( QString host, int port )
{
  ConnectNetworkService *cns = new ConnectNetworkService(host.latin1(), port);
/*   cns->start(); */
  ListViewServiceItem *rootlistview = new ListViewServiceItem(listViewSources, "Marketplace: "+host+":"+QString().setNum(port));
  DataReceivedListener *drl = new ServiceFoundLister(rootlistview);
  cns->get("<agent agent=\"all\"><search></search></agent>", drl);  
}

void FormView::removeFromDrawList( Draw *draw )
{
  for(int nr=0; nr<listBoxDrawItems->count(); nr++) {
    ListBoxDrawItem *item = (ListBoxDrawItem *) listBoxDrawItems->item(nr);
    if (item->getDraw()==draw) {
/*       goc->remove(draw); */
      delete(item);
      nr=0;
    }
  }
}


void FormView::infoSlot()
{
  forminfo->updateInfo();
  forminfo->show();
}


void FormView::addSlot()
{
  formaddsource->show();
}

bool FormView::event( QEvent *e )
{
  if (e->type()==QEvent::User) {
    InsertListViewItemEvent *ievent = (InsertListViewItemEvent *) e;
    ievent->createAndInsertItem(gLMainWidget);
    return(TRUE);
  }
  else if (e->type()==(QEvent::Type) 1001) {
    InsertListDrawItemEvent *ievent = (InsertListDrawItemEvent *) e;
    addToDrawList(ievent->getDraw());
    return(TRUE);
  }
  else if(e->type()==(QEvent::Type) 1002) {
    URLRawDownloadEvent *uevent = (URLRawDownloadEvent *) e;
    uevent->getURLRawDownload()->run();
    return(TRUE);
  }
  else if(e->type()==(QEvent::Type) 1003) {
    DownloadFinishedEvent *devent = (DownloadFinishedEvent *) e;
    devent->getURLDownload()->finished_download(NULL);
    return(TRUE);
  }
  else if(e->type()==(QEvent::Type) 1004) {
    MoveToPositionEvent *mevent = (MoveToPositionEvent *) e;
    gLMainWidget->moveToPosition(mevent->getMark(), mevent->getDir(), mevent->getUp());
    return(TRUE);
  }
  else {
    return(QMainWindow::event(e));
  }
}

void FormView::movieStartStopSlot( bool status )
{
  if (status) {
    connect(gLMainWidget, SIGNAL(frameFinished()), this, SLOT(captureFrameSlot()));
  }
  else {
    disconnect(gLMainWidget, SIGNAL(frameFinished()), this, SLOT(captureFrameSlot()));
  }
}

void FormView::captureFrameSlot()
{
  gLMainWidget->screenshotSlot(QString("movie")+(QString("000000")+QString().setNum(movieframecount)).right(6)+".png");
  statusBar()->message("Captured frame "+QString().setNum(movieframecount), 1000);

  movieframecount++;
}


void FormView::destroy()
{
::exit(0);
}


void FormView::optionsSlot()
{
  FormOptions fo;
  /* set current values */
  fo.setHeightfieldMultiplier(heightfieldmultiplier);
  fo.setEarthView(maptiledrawtype);
  fo.setProxy(getAttribute("proxyused")=="true", getAttribute("proxy"), getAttribute("proxyport").toInt());
  fo.setUseMultitexturing(getAttribute("usemultitexturing", "true")=="true");
  fo.setUseTextureCompression(getAttribute("usetexturecompression", "true")=="true");
  fo.setUseLocalCaching(getAttribute("usecache", "true")=="true");
  fo.setLocalCacheSize(getAttribute("cachesize", "50").toInt());
  fo.setLocalCacheLocation(getAttribute("cachelocation", QDir::homeDirPath()+QDir::separator()+QString(".earth3d")+QDir::separator()+QString("cache")));
  fo.setLocalCacheUsage(fileCache.getUsedSizeMB(), getAttribute("cachesize", "50").toInt());
  fo.setRotationType(getAttribute("rotationtype").toInt());

  cout << "exec" << endl;
  int result = fo.exec();
  cout << "exec2" << endl;
  if (result==QDialog::Accepted) {
    /* read new values */
    heightfieldmultiplier = fo.getHeightfieldMultiplier();
    maptiledrawtype = fo.getEarthView();
    needRedraw = true;

    setAttribute("proxyused", fo.getProxyUsed() ? "true":"false");
    setAttribute("proxy", fo.getProxyName());
    setAttribute("proxyport", QString::number(fo.getProxyPort()));
    setAttribute("usemultitexturing", fo.getUseMultitexturing() ? "true":"false");
    setAttribute("usetexturecompression", fo.getUseTextureCompression() ? "true":"false");
    setAttribute("usecache", fo.getUseLocalCaching() ? "true":"false");
    setAttribute("cachesize", QString::number(fo.getLocalCacheSize()));
    setAttribute("cachelocation", fo.getLocalCacheLocation());
    setAttribute("rotationtype", QString::number(fo.getRotationType()));

    saveAttributes();
    loadCachedAttributes();
    fileCache.setCacheLocation(getAttribute("cachelocation", QDir::homeDirPath()+QDir::separator()+QString(".earth3d")+QDir::separator()+QString("cache")));
  }
}

void FormView::setServiceListLocation(QString location) {
  serviceListLocation = location;
}
