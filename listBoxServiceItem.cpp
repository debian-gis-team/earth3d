#include "listBoxServiceItem.h"
#include <qdom.h>

ListBoxServiceItem::ListBoxServiceItem ( QDomNode text, const char *connection, const char *sender )
  : QListBoxText() {
  const char *prop = text.toElement().attribute("name");
  setText(prop);

  if (connection!=NULL) {
    this->connection = new char[strlen(connection)+1];
    strcpy(this->connection, connection);
  }
  else {
    this->connection = NULL;
  }

  if (sender!=NULL) {
    this->sender = new char[strlen(sender)+1];
    strcpy(this->sender, sender);
  }
  else {
    this->sender = NULL;
  }
}

ListBoxServiceItem::~ListBoxServiceItem () {
  if (connection) delete[](connection);
  if (sender) delete[](sender);
}

char *ListBoxServiceItem::getConnection() {
  return(connection);
}

char *ListBoxServiceItem::getSender() {
  return(sender);
}
