#include "mapTileUncompressed.h"
#include <stdlib.h>
#include <png.h>
#include "pngutils.h"
#include "jpgutils.h"
#include <assert.h>
#include <math.h>
#include "statusObserverQT.h"

MapTileUncompressed::MapTileUncompressed() : MapTile() {
  bpp = 3;
  uncompressedimage = NULL;
  uncompressedsize = 0;
  scaledImage = NULL;
}

MapTileUncompressed::~MapTileUncompressed() {
  discardUncompressedImage();
  discardScaledImage();
}

void MapTileUncompressed::generateUncompressedImage() {
  discardUncompressedImage();

  /* if there is no compressed image, we need no uncompressed image */
  if (image==NULL) {
#ifdef EARTH3DDEBUG
    printf("tile.image=NULL\n");
#endif
    uncompressedimage = NULL;
    return;
  }

  /* create uncompressed image */
  uncompressedimage = new char[width*height*getBPP()];
  uncompressedsize = width*height*getBPP();
  statusobserver.changeMemoryOffset(StatusObserver::MEM_UNCOMPRESSED, float(uncompressedsize));

  if (strcmp(type, "PNG")==0) {
    // check for compressed image
    if (png_sig_cmp((png_bytep) image, (png_size_t)0, 4)==0) {
      // PNG image
#ifdef EARTH3DDEBUG
      printf("PNG decompress\n");
#endif
      PNGUtils::readPNG(image, imagesize, uncompressedimage, uncompressedsize);
      return;
    }
  }

  if (strcmp(type, "JPG")==0) {
    // check for compressed image
#ifdef EARTH3DDEBUG
    printf("JPG decompress\n");
#endif
    JPGUtils::readJPG(image, imagesize, uncompressedimage, uncompressedsize);
    return;
  }
  
  // fallback
  assert(imagesize == uncompressedsize);
  memcpy(uncompressedimage, image, imagesize > uncompressedsize ? uncompressedsize:imagesize);
}

char *MapTileUncompressed::getUncompressedImage() {
  QMutexLocker qml(&uncompressingMutex);
  if (!uncompressedimage) {
    generateUncompressedImage();
  }

  return(uncompressedimage);
}

int MapTileUncompressed::getUncompressedImageSize() {
  return(width*height*getBPP());
}

void MapTileUncompressed::discardUncompressedImage() {
  if (uncompressedimage) {
    delete[](uncompressedimage);
    statusobserver.changeMemoryOffset(StatusObserver::MEM_UNCOMPRESSED, -float(uncompressedsize));
    uncompressedimage = NULL;
  }
}

void MapTileUncompressed::generateScaledImage() {
  if (!scaledgenerated) {
    if ((width==256 || width==128 || width==64 || width==32 ||
	 width==16 || width==8 || width==4 || width==2 ||
	 width==1) &&
	(height==256 || height==128 || height==64 || height==32||
	 height==16 || height==8 || height==4 || height==2 ||
	 height==1)) {
      scaledWidth = width;
      scaledHeight = height;
    }
    else {
      if (scaledImage) {
	delete[](scaledImage);
	statusobserver.changeMemoryOffset(StatusObserver::MEM_SCALED, -scaledWidth*scaledHeight*3);
      }
      scaledWidth = pow(2., int(log(double(width))/log(2.)+1));
      scaledHeight = pow(2., int(log(double(height))/log(2.)+1));
#ifdef EARTH3DDEBUG
      printf("\n\nPOW: %i %i\n\n", scaledWidth, scaledHeight);
#endif
      scaledImage = new char[scaledWidth*scaledHeight*3];
      statusobserver.changeMemoryOffset(StatusObserver::MEM_SCALED, scaledWidth*scaledHeight*3);
      char *image = (char *) getUncompressedImage();
      float sx = float(width)/scaledWidth;
      float sy = float(height)/scaledHeight;
      for(int ny=0; ny<scaledHeight; ny++) {
	for(int nx=0; nx<scaledWidth; nx++) {
	  scaledImage[(ny*scaledWidth+nx)*3+0]=image[int(int(ny*sy)*width+int(nx*sx))*3+0];
	  scaledImage[(ny*scaledWidth+nx)*3+1]=image[int(int(ny*sy)*width+int(nx*sx))*3+1];
	  scaledImage[(ny*scaledWidth+nx)*3+2]=image[int(int(ny*sy)*width+int(nx*sx))*3+2];
	}
      }
    }

    scaledgenerated = true;
  }
}

void MapTileUncompressed::discardScaledImage() {
  if (scaledImage) {
    delete[](scaledImage);
    statusobserver.changeMemoryOffset(StatusObserver::MEM_SCALED, -scaledWidth*scaledHeight*3);
    scaledImage = NULL;
    scaledgenerated = false;
  }
}

void MapTileUncompressed::setBPP(int newbpp) {
  bpp = newbpp;
}

int MapTileUncompressed::getBPP() {
  return(bpp);
}

void MapTileUncompressed::setImage(const char *newimage, const char *type, int size) {
  MapTile::setImage(newimage, type, size);

  scaledgenerated = false;
  discardScaledImage();
}

char *MapTileUncompressed::getScaledUncompressedImage() {
  generateScaledImage();

  if (scaledImage) {
    return(scaledImage);
  }
  else {
    return(getUncompressedImage());
  }
}

int MapTileUncompressed::getScaledWidth() {
  return(scaledWidth);
}

int MapTileUncompressed::getScaledHeight() {
  return(scaledHeight);
}

