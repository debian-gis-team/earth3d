#include "statusObserverQT.h"
#include "formstatus.h"
#include <qlabel.h>
#include <assert.h>
#include <stdlib.h>

StatusObserverQT statusobserver;

extern FormStatus *fstatus;

StatusObserverQT::StatusObserverQT() : StatusObserver() {
}

StatusObserverQT::~StatusObserverQT() {
}

void StatusObserverQT::changeMemoryOffset(enum StatusObserverMemorySet soms, float difference) {
  StatusObserver::changeMemoryOffset(soms, difference);

  assert(abs(difference)<10000000);

  updateDisplay();
}

void StatusObserverQT::updateDisplay() {
  if (!fstatus) return;
  fstatus->textLabelUncompressed->setText(QString().setNum(memUncompressed, 'g', 12));
  fstatus->textLabelScaled->setText(QString().setNum(memScaled, 'g', 12));
  fstatus->textLabelTextures->setText(QString().setNum(memTexture, 'g', 12));
  fstatus->textLabelVertex->setText(QString().setNum(memVertex, 'g', 12));
  fstatus->textLabelTexcoord->setText(QString().setNum(memTexcoord, 'g', 12));
  fstatus->textLabelUrlDownloads->setText(QString().setNum(memUrlDownload, 'g', 12));
}
