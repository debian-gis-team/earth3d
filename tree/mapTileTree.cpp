#include "mapTileTree.h"

MapTileTree::MapTileTree() {
  rootNode = new MapTileTreeNode(NULL);
}

MapTileTree::~MapTileTree() {
  if (rootNode) delete(rootNode);
}

MapTileTreeNode *MapTileTree::getRootNode() {
  return(rootNode);
}

void MapTileTree::garbageCollectNode(MapTileTreeNode *node) {
  node->garbageCollect();
  for(int i=0; i<4; i++) {
    MapTileTreeNode *child = node->getChild(i);
    if (child) garbageCollectNode(child);
  }
}

void MapTileTree::garbageCollect() {
  garbageCollectNode(getRootNode());
}

