#ifndef MAPTILEUNCOMPRESSED_H
#define MAPTILEUNCOMPRESSED_H

#include "mapTile.h"
#include <qmutex.h>

class MapTileUncompressed : public MapTile {
 protected:
  /* create an uncompressed image from the PNG or JPG image */
  virtual void generateUncompressedImage();

  int uncompressedsize;
  char *uncompressedimage;
  QMutex uncompressingMutex;

  /* create an image that is scaled to 2^n */
  virtual void generateScaledImage();
  int scaledWidth, scaledHeight;
  char *scaledImage;
  bool scaledgenerated;

  int bpp;

 public:
  MapTileUncompressed();
  virtual ~MapTileUncompressed();

  virtual void setImage(const char *newimage, const char *type, int size);

  char *getUncompressedImage();
  int getUncompressedImageSize();
  void discardUncompressedImage();

  char *getScaledUncompressedImage();
  int getScaledWidth();
  int getScaledHeight();
  void discardScaledImage();

  void setBPP(int newbpp);
  int getBPP();
};

#endif
