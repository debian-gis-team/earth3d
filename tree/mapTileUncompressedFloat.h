#ifndef MAPTILEUNCOMPRESSEDFLOAT_H
#define MAPTILEUNCOMPRESSEDFLOAT_H
#include "mapTileUncompressed.h"

class MapTileUncompressedFloat : public MapTileUncompressed {
 protected:
  bool littleendian;

  virtual void generateUncompressedImage();

  virtual void generateScaledImage();

 public:
  MapTileUncompressedFloat();
  virtual ~MapTileUncompressedFloat();
};

#endif
