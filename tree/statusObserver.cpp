#include "statusObserver.h"

StatusObserver::StatusObserver() {
  memTexture = 0;
  memUncompressed = 0;
  memScaled = 0;
  memVertex = 0;
  memTexcoord = 0;
  memUrlDownload = 0;
}

StatusObserver::~StatusObserver() {
}

void StatusObserver::changeMemoryOffset(enum StatusObserverMemorySet soms, float difference) {
  switch(soms) {
  case MEM_TEXTURE:
    memTexture += difference;
    break;
  case MEM_UNCOMPRESSED:
    memUncompressed += difference;
    break;
  case MEM_SCALED:
    memScaled += difference;
    break;
  case MEM_VERTEX:
    memVertex += difference;
    break;
  case MEM_TEXCOORD:
    memTexcoord += difference;
    break;
  case MEM_URLDOWNLOAD:
    memUrlDownload += difference;
    break;
  }
}
