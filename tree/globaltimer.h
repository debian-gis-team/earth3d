#ifndef GLOBALTIMER_H
#define GLOBALTIMER_H

/* saves a current time/frame number for use with the garbage collection */
extern unsigned long globaltime;

#endif
