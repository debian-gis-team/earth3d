#ifndef REQUESTIDNODE_H
#define REQUESTIDNODE_H
#include <qmutex.h>

class RequestIDNode {
  /** xml connections strings to request this node */
  char *requestID; 
  /** xml connections strings to request the child nodes */
  char *childRequestIDs[4]; 

  QMutex requestIDmutex;

public:
  // DEBUG
  long magic;

  RequestIDNode();
  virtual ~RequestIDNode();

  void setRequestID(const char *requestID);
  char *getRequestID();

  void setChildRequestID(int nr, const char *requestID);
  char *getChildRequestID(int nr);
};

#endif
