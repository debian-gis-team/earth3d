#ifndef HEIGHTFIELDTILETREENODECORE_H
#define HEIGHTFIELDTILETREENODECORE_H
#include "mapTileUncompressedFloat.h"
#include "mapTileTreeNodeCore.h"
#include "point3d.h"
#include "geometry2d3d.h"
#include "rect2d.h"
#include "point2dint.h"
#include <qmutex.h>
#include "globalsettings.h"

#define HEIGHTFIELDWIDTH  32
#define HEIGHTFIELDHEIGHT 32

extern QMutex heightfieldtreenodecorechanges;

class HeightfieldTreeNodeCore : public MapTileTreeNodeCore {
  /** every tile has an additional border of one pixel at the right and
   * the bottom of the tile to close the gaps to the next tile. In this array
   * the original border heights are saved to be reused for new calculations.
   *
   * The borders left and up are backuped and overwritten with data from other
   * tile. For the borders right and down an extra column/row is added in setImage.
   */
  float *heightborder[2];

  /** the number of data points for each of the four borders. The values itself are saved in tile.image, but
      the render function generateVertexArray uses this value to generate GL_TRIANGLE_FANs at the borders
      to fit the different resolution of two adjacent tiles */
  int width[4];

  Rect2D rect;

  void extendImage(int width, int height, const char *image);

  /** is set to true when the vertex array needs to be rebuild */
  bool dirty;

  bool dirtyBorders;

  /** is set when the neighbors were informed about a change in this heightfield */
  bool informedNeighbors;

  /** is set to true when the inner vertices are already generated */
  bool innerGenerated;
  int oldBeginx, oldEndx, oldBeginy, oldEndy;

  int lastDataRow, lastDataColumn;

  /** The basic geometry data to which the heightfield is mapped with generateVertexArray */
  Geometry2D3D *geometry;
  float radius;

 protected:
  /** Creates the tile MapTile. */
  virtual void createMapTile();

  /** interpolation vertex array, contains difference vectors from the coarse data to the fine data */
  unsigned int vertexinterpolationarraysize;
  /** contains difference values */
  Point3D *vertexinterpolationarray;
  /** contains old heightfield values */
  Point3D *vertexbackuparray;
  Rect2D vertexbackuparraydatarect;
  int vertexinterpolationarraywidth;
  unsigned int tmpvertexarraysize;
  Point3D *tmpvertexarray; // used by getInterpolationVertexArray

 public:
  // DEBUG FIXME
  bool markedfordebug;
  long magicdebug;

  Point3D vertex[4];

  /* data for the vertices inside the heightfield */
  int vertexcount;
  unsigned int vertexarraysize;
  unsigned int stripsize;
  Point3D *vertexarray;
  Point2D *texcoordarray;
  Rect2D vertexarraydatarect;
  QMutex vertexarraymutex;

#ifdef RELATIVETRANSFORM
  /** Defines the origin for this vertex */
  Point3D vertexorigin;
  /** Defines the scale factor for this vertex */
  double vertexscale;
#endif

  /* data for the vertices at the border of the heigtfield */
  int fancount[4];
  int *fansize[4];
  Point3D *fanvertexarray[4];
  Point2D *fantexcoordarray[4];

  HeightfieldTreeNodeCore();
  virtual ~HeightfieldTreeNodeCore();
  virtual void setVertex(int nr, Point3D p);
  virtual void setImage(int width, int height, const char *image, int size, const char *type);
  virtual void setRect(Rect2D *rect);
  Rect2D *getRect();
  virtual void setBaseGeometry(Geometry2D3D *geometry, float radius);
  virtual void generateVertexArray();

  void generateBorder(int direction, int level, int offset, HeightfieldTreeNodeCore *otherhcore);

  static Point2DInt getCornerCoordinates(int direction, int maxx, int maxy);

  /** Generates the value for the one corner specified with direction by using the corner
   *  of the other tile, othercore, specified by othertiledirection. 
   */
  void generateCorner(int direction, int level, Point2DInt offset, int othertiledirection, HeightfieldTreeNodeCore *otherhcore, int matchingborder);
  float getCorner(int x, int y);
  void setCorner(int x, int y, float value);

  void restoreBorder(int direction);
  float *getBorder(int direction);

  int getLastDataRow();
  int getLastDataColumn();

  int getBPP();

  bool isDirtyBorders();
//  void test(int direction);

  void resetInformedNeighbors();
  bool getInformedNeighbors();

  virtual bool getThisInterpolates();
  virtual void finishInterpolation();

  /** @param interpolationpos is the interpolation value between 0 (high) and 1 (low)
   *  @returns the array of interpolated heightfield coordinates
   */
  Point3D *getInterpolationVertexArray(float interpolationpos);

  /** Garbage collect */
  virtual void garbageCollect();
};

#endif
