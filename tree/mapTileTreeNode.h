#ifndef MAPTILETREENODE_H
#define MAPTILETREENODE_H
#include <vector>
#include "mapTileTreeNodeCore.h"
#include "point2dint.h"
#include "boundingSphere.h"

#define INTERPOLATIONFRAMES 250

using namespace std;

class MapTileTreeNode;

struct MapPart {
  /** the difference in level between the searched and the current node. 0 means the are on
   * the same level and therefore have a common border. 1 means that from the common border
   * you can only use one half since the tile is twice as large. And so on...
   */
  int level;

  /** The node that was found in the level distance. */
  MapTileTreeNode *node;

  /** the offset into the border. 0 means that the current tile has its common border at the upper
   * left corner of the other tile. Other values mean that it has an offset of 
   * (borderwith/(2^level))*offset
   */
  Point2DInt offset;

  MapPart() {
    level = 0;
    node = NULL;
    offset = Point2DInt(0,0);
  };

  MapPart(int level, MapTileTreeNode *node, Point2DInt offset) {
    this->level = level;
    this->node = node;
    this->offset = offset;
  };
};

/** This class represents one node in the data tree for the MapTile objects for
 *  one geometry object. It contains one core object that contains the data.
 */
class MapTileTreeNode {
  /** the 4 childs of each node, may be NULL.
   * Stores four quadrants of a map in the ntree in the following order:
   * 0=right,up 1=right,down 2=left,up 3=left,down
   */
  MapTileTreeNode *child[4];
  /* holds the content of this node */
  MapTileTreeNodeCore *cores[2];

  MapTileTreeNode *parent;

  /** The internal time/frame number when this node was last needed for the scene. It
      also counts as needed when a node that is under this node in the tree was needed
      for the view.
  */
  unsigned long lastNeeded;

  /** This is selected when it gets in the drawing list for the current frame. */
  bool selectedForDrawing;

  /** This is selected when this node was selected for drawing in the last frame. */
  bool wasSelectedForDrawing;

  /** This is selected by TreeDrawSphere when a node is visible for the viewer (e.g. in view frustum). */
  bool visible;

  /** @returns the number this node has as childnr at its parent */
  int getThisChildnr();

  BoundingSphere bs;

  /** Counts how many frames are left until the interpolation is finished */
  int interpolationcounter;
 protected:
  /** Determines if one of the childs interpolates */
  bool childInterpolates;

  /** Determines if this tile interpolates */
  bool thisInterpolates;

  /** Determines the interpolation direction */
  bool interpolationDirectionIn;

 public:
  // DEBUG FIXME
  int depth;

  MapTileTreeNode(MapTileTreeNode *parent);
  virtual ~MapTileTreeNode();

  /** Returns, depending on the childnr the coordinates (1,0), (1,1), (0,0) or (0,1) */
  Point2DInt getSimpleTileCoordinates(int childnr);
  /** Returns, depending on the direction (clockwise) the coordinates (1,0), (0,1), (-1,0), (0,-1) */
  Point2DInt getSimpleTileDirections(int direction);
  /** Returns the childnr for the given simple coordinates returned by getSimpleTileCoordinates. */
  int getChildnr(Point2DInt p);

  MapTileTreeNode *getChild(int nr);
  /** @returns true if it has at least one child */
  bool hasAnyChild();
  void setChild(int nr, MapTileTreeNode *child);
  void deleteChild(int nr);

  bool getRequested();

  bool getGenerated();

  int getCoreCount();
  MapTileTreeNodeCore *getCore(int nr);
  void setCore(int nr, MapTileTreeNodeCore *core);

  MapTileTreeNode *getParent();

  void setSelectedForDrawing(bool select);
  bool getSelectedForDrawing();

  void setWasSelectedForDrawing(bool select);
  bool getWasSelectedForDrawing();

  void setVisible(bool select);
  bool getVisible();

  /** get neighbor on higher or same level */
  MapPart getNeighbor(int direction);

  MapPart getCornerNeighbor();

  /** get selected neighbor on higher or same level. If none is selected, it returns the deepest neighbor */
  MapPart getSelectedNeighbor(int direction);

  /** Inform the direct neighbors that this node has changed and been selected for drawing
   *  and that they need to adjust their heightfield borders
   */
  void informNeighborNodes();

  /** Corrects the heigtfield values at the border in direction direction with the values
   *  from node. If this node here has children, they are adjusted to the values of node. It
   *  goes down to the children until it finds nodes that are selected for drawing.
   */
  void regenerateHeightfield(MapTileTreeNode *node, int direction, int level=0, Point2DInt offset = Point2DInt(0,0));

  BoundingSphere *getBoundingSphere();

  void setBoundingSphere(BoundingSphere bs);
  
  /** Generate the bounding sphere from the vertex data of the texture and the
   *  heightfield data. 
   */
  void generateBoundingSphere();
  
  virtual void garbageCollect();

  void setLastNeeded(unsigned long timenr);

  /** Interpolate between different levels in the tree.
   *  @returns true if one of the childs has some interpolation left to do.
   */
  bool getChildInterpolates();

  /** Interpolate between different levels in the tree.
   *  @returns true if this node has some interpolation left to do.
   */
  bool getThisInterpolates();

  /** Checks the interpolation state of its cores. Needs to be called at every
   *  frame to see if there was a completed download between these frames
   */
  void checkThisInterpolates();

  /** Returns the current interpolation position between 0 and 1. */
  float getInterpolationCounter();

  /** Decreases the current interpolation counter by one frame */
  void decreaseInterpolationCounter();

  /** Reset the interpolation to the start. Used when a maptile reappears. */
  void resetInterpolation();

  /** Set the interpolation to ingoing or outcoming */
  void setInterpolationDirection(bool in);
  
  /** Set the interpolation counter (0-1) */
  void setInterpolationCounter(float pos);
};

#endif
