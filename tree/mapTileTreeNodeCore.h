#ifndef MAPTILETREENODECORE_H
#define MAPTILETREENODECORE_H
#include "mapTileUncompressed.h"
#include "requestIDNode.h"
#include "downloadable.h"
#include "stopableDownload.h"
#include "dataReceivedMapTile.h"

/** This is the core of a MapTileTreeNode. It contains the data that belongs to
 *  one node of the tree, including a MapTile object with that map itself. But it
 *  also contains information from where to request the data for this node and if
 *  the request is already running.
 */
class MapTileTreeNodeCore : public RequestIDNode, public DataReceivedMapTile {
  /** determines if there is already a request for this child running */
  bool requested;
  /** determines if this data was generated. When it was generated, it is
   * normally also requested and replaced by the received data when it arrives.
   */
  bool generated;

  /** determines if a download attempt has been done and has finished */
  bool downloaded;

 protected:
  /** This MapTile contains the real data from the server, the compressed image. */
  MapTileUncompressed *tile;

  QMutex uncompressedimagemutex;

  /** Sets this NodeCores MapTile to the given content. Used in texture interpolation. */
  void setMapTile(MapTile &newtile);

  /** Creates the tile MapTile. */
  virtual void createMapTile();

  bool downloadFailed;

 public:
  MapTileTreeNodeCore();
  virtual ~MapTileTreeNodeCore();

  virtual void addDownloader(StopableDownload *drl);

  void setRequested(bool requested);
  bool getRequested();
  
  void setGenerated(bool generated);
  bool getGenerated();

  void setDownloaded(bool downloaded);
  /** @returns true if a download attempt has already be done 
   */
  bool getDownloaded();

  /** This method is called when the download has finally failed after serveral attempts */
  virtual void setDownloadFailed();
  virtual bool getDownloadFailed();

  virtual void setImage(int width, int height, const char *image, int size, const char *type);

  virtual bool hasImage();

  virtual int getWidth();
  virtual int getHeight();

  virtual char *getUncompressedImage();
  virtual int getUncompressedImageSize();
  virtual char *getCompressedImage();
  virtual int getCompressedImageSize();
  virtual void discardUncompressedImage();
  
  /** @returns the bytes per pixel */
  virtual int getBPP();

  virtual void removeMapTile();

  /** Interpolation between two different levels in the tree */
  virtual bool getThisInterpolates() = 0;

  /** Stops interpolation by setting the new image/data as main image/data
   *  and setting thisInterpolates to false
   */
  virtual void finishInterpolation() = 0;

  /** Garbage collect */
  virtual void garbageCollect();

  char *getScaledUncompressedImage();
  int getScaledWidth();
  int getScaledHeight();
};

#endif
