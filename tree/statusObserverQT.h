#ifndef STATUSOBSERVERQT_H
#define STATUSOBSERVERQT_H
#include "statusObserver.h"

class StatusObserverQT : public StatusObserver {
 public:
  StatusObserverQT();
  virtual ~StatusObserverQT();

  virtual void changeMemoryOffset(enum StatusObserverMemorySet soms, float difference);

  void updateDisplay();
};

extern StatusObserverQT statusobserver;

#endif
