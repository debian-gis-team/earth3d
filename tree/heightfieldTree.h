#ifndef HEIGHTFIELDTREE_H
#define HEIGHTFIELDTREE_H
#include "heightfieldTreeNodeCore.h"
#include "heightfieldTreeNode.h"
#include "connectNetworkService.h"
#include "dataReceivedMapTile.h"

extern QMutex heightfieldTreeMutex;

class HeightfieldTree {
  /** The root node of this heightfield tree */
  HeightfieldTreeNode *rootNode;
  /** The network service over that the maptiles are requested */
  ConnectNetworkService *cns;
  /** DataReceivedListener that received the heightfield map tiles */
  DataReceivedMapTile *drmt;
  
  /** Starts a new request for a heightfield map tile */
  void requestFromNetwork(HeightfieldTreeNode *hnode);
  /** @returns the root node of the heightfield tree */
  HeightfieldTreeNode *getRootNode();

  void garbageCollectNode(HeightfieldTreeNode *node);

public:
  /** Creates a new heightfield tree that exists beside the MapTileTree for requesting
   *  heightfield data with a different tile size than that is needed by the MapTileTree's
   *  heightfield map tiles.
   *  param rootRequestID Sets a request ID for the root node of this heightfield tree. This heightfield tree
   *  requests this rootRequestID for its rootNode. It then gets the needed data to request
   *  further heightfield nodes.
   */
  HeightfieldTree(ConnectNetworkService *cns, char *rootRequestID);
  ~HeightfieldTree();
  
  /** Request a heightfield from this tree. It tests if it needs to get more data from
   *  the network or if it can generate the data from the already downloaded data.
   */
  void request(HeightfieldTreeNodeCore *hcore);

  /** Removes unneccessary parts of the tree.
   */
  void garbageCollect();
};

#endif
