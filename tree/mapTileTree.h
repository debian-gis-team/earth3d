#ifndef MAPTILETREE_H
#define MAPTILETREE_H
#include "mapTileTreeNode.h"

class MapTileTree {
  MapTileTreeNode *rootNode;
  
  void garbageCollectNode(MapTileTreeNode *node);
  
 public:
  MapTileTree();
  ~MapTileTree();

  MapTileTreeNode *getRootNode();
  
  /** This method calls the garbageCollect method of every node in the tree to free
   *  unneeded memory.
   */
  void garbageCollect();
};

#endif
