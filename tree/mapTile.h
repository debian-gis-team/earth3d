#ifndef MAPTILE_H
#define MAPTILE_H

class MapTile {
 public:
  int width, height;
  int imagesize;

  /** This array contains the original compressed image data. */
  char *image; 
  char *type;

  MapTile();
  virtual ~MapTile();

  virtual void setImage(const char *image, const char *type, int size);
  /** Clears this object and frees the image memory. */
  void freeImage();
  /** @returns true if there is an image saved in this MapTile */
  bool hasImage();
  /** Takes the data and memory of new and deletes its pointers. */
  void take(MapTile &newtile);
};

#endif
