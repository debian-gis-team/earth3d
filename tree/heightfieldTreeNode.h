#ifndef HEIGHTFIELDTREENODE
#define HEIGHTFIELDTREENODE
#include "heightfieldTreeNodeCore.h"
#include "heightfieldTreeNode.h"
#include <vector>
#include <qmutex.h>

class HeightfieldTree;
class StopableDownload;

using namespace std;

class HeightfieldTreeNode : public HeightfieldTreeNodeCore, public StopableDownload {
  /** the 4 childs of each node, may be NULL.
   * Stores four quadrants of a map in the ntree in the following order:
   * 0=right,up 1=right,down 2=left,up 3=left,down
   */
  HeightfieldTreeNode *child[4];

  /** The parent tree */
  HeightfieldTree *htree;
  /** The parent node */
  HeightfieldTreeNode *parent;

  vector<HeightfieldTreeNodeCore *> hcorelist;
  QMutex hcorelistmutex;

  bool littleendian;

  /** The global time this heightfield was last used. */
  unsigned int lastused;
  /** The global time one of its childs was used. */
  unsigned lastchildused;

  /** Removes an object from the queue. This is used when the object
   *  is being deleted and does not need the data any longer.
   *  @param hcore the HeightfieldTreeNodeCore to be dequeued  
   */
  void dequeueRequest(HeightfieldTreeNodeCore *hcore);

  /** Rerequests all objects that have been waiting for the download
   *  of this object.
   */
  void RerequestObjects();

 protected:

  /** @returns true if there is no request waiting for this node or any of its childs */
  bool canBeDeleted();

 public:
  HeightfieldTreeNode(HeightfieldTree *htree, HeightfieldTreeNode *parent);
  virtual ~HeightfieldTreeNode();
  
  HeightfieldTreeNode *getChild(int nr);
  void setChild(int nr, HeightfieldTreeNode *hnode);

  /** Enqueue a request. The specified hcore is filled with data when
   *  this object finished loading.
   *  @param hcore the HeightfieldTreeNodeCore to be enqueued
   */  
  void enqueueRequest(HeightfieldTreeNodeCore *hcore);

  virtual void setImage(int width, int height, const char *image, int size, const char *type);
  
  void getArea(int x, int y, int width, int height, float *target);

  /** Garbage collect */
  virtual void garbageCollect();

  /** Sets the usage time of this node */
  void setLastUsed(unsigned long timenow);
  
  /** Sets the last child usage time of this node and all parents */
  void setLastChildUsed(unsigned long timenow);

  /** Stop the current download 
   *  @downloadable the Downloadable objects whos download should be stopped
   */
  virtual void stopDownload(Downloadable *downloadable);

  virtual void setDownloadFailed();
};

#endif
