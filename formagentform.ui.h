/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include <qlabel.h>
#include <qlineedit.h>
#include <qdom.h>
#include <iostream>

void FormAgentForm::init() {
}

void FormAgentForm::setXMLInputs( QString xmlform, QMap<QString, QString> propertiesMap )
{
  agentGrid = new QGridLayout(groupBoxAgent, 0, 0, 5);
  agentGrid->setAutoAdd(false);

  int currentrow=0;
  // parse the document
  QDomDocument doc;
  if (doc.setContent(QString::fromLatin1(xmlform))) {
    /* search for form tag */
    QDomNode nform = doc.documentElement();
    while( !nform.isNull() ) {
      QDomElement e = nform.toElement(); // try to convert the node to an element.
      if( !e.isNull() && e.tagName() == QString("form")) {
	/* iterate through input tags */
	QDomNode ninput = nform.firstChild();
	while( !ninput.isNull() ) {
	  e = ninput.toElement(); // try to convert the node to an element.
	  if( !e.isNull() && e.tagName() == QString("input")) {
	    /* insert input field */
	    agentGrid->addWidget(new QLabel(e.attribute("label", ""), groupBoxAgent), currentrow, 0);
	    QString value = e.attribute("value", "");
	    if (propertiesMap.contains(e.attribute("name",""))) {
	      value = propertiesMap[e.attribute("name","")];
	    }

	    QLineEdit *ledit = new QLineEdit(value, groupBoxAgent);
	    agentGrid->addWidget(ledit, currentrow, 1);
	    widgetlist.push_back(WidgetArray(e.attribute("name", ""), ledit));

	    currentrow++;
	  }

	  ninput = ninput.nextSibling();
	}
      }
      nform = nform.nextSibling();
    }
  }

  widgetlist[0].widget->setFocus();

  resize( QSize(100, 100).expandedTo(minimumSizeHint()) + QSize(0,10) );
  clearWState( WState_Polished );
}


void FormAgentForm::insertXMLResult(QDomDocument &doc, QDomNode &node)
{
  /* iterate through widgetlist, read results and generate xml document */
  QValueList<WidgetArray>::iterator i;
  for(i=widgetlist.begin(); i!=widgetlist.end(); i++) {
    WidgetArray *wa = &(*i);
    QDomElement element = doc.createElement("parameter");
    element.setAttribute("name", wa->widgetname);
    QLineEdit *ledit = (QLineEdit *) wa->widget;
    element.setAttribute("value", ledit->text());

    node.appendChild(element);
  }

  QString output;
  QTextStream ts(&output, IO_WriteOnly);
  ts << doc;
  std::cout << "doc: " << output << std::endl;
}


void FormAgentForm::buttonOKSlot()
{
  hide();

  /* insert result into connection string */
  QString connectionresult = insertIntoConnections(connection);

  std::cout << "xml connection result content: " << std::endl << connectionresult.latin1() << std::endl;

  /* request object */
  cns->getOne(connectionresult.latin1(), drl);

  /* close this window */
  close(TRUE);
}

void FormAgentForm::setRetrievalSettings(ConnectNetworkService *cns, const char *connection, DataReceivedListener *drl) {
  this->cns = cns;
  this->connection = QString(connection);
  this->drl = drl;
}

QString FormAgentForm::insertIntoConnections(QString connections) {
  /* create DOM structure */
  QDomDocument doc;
  if (doc.setContent(QString(connections))) {
    QDomNode cur = doc.documentElement();
    if (cur.isElement() && cur.toElement().tagName() == QString("connections")) {
      QDomNode start = cur;

      /* search for agent or url connection */
      cur = start.firstChild();
      while(!cur.isNull()) {
	if (cur.isElement()) {
	  /* insert resultdata */
	  insertXMLResult(doc, cur);
	}

	cur = cur.nextSibling();
      }
    }

    QString output;
    QTextStream ts(&output, IO_WriteOnly);
    ts << doc;
    
    return(output);
  }

  return(connection);
}
