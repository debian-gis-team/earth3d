earth3d (1.0.5-3) unstable; urgency=low

  * Team upload.
  * debian/rules:
    - Ensure quilt patches are correclty applied before build starts.
  * debian/control:
    - Add Vcs-* fields.

 -- Aron Xu <aron@debian.org>  Mon, 27 Jun 2011 20:52:20 +0800

earth3d (1.0.5-2) unstable; urgency=low

  * Team upload by Debian GIS Project.
  * Use 3.0 (quilt) format, dh compact 7, covert patches to quilt.
  * debian/copyright: 
    - Reference to GPL-2 instead of obsolete GPL.
    - Add valid copyright info.
  * debian/earth3d.desktop: 
    - Fix typo (Closes: #550034).
    - Remove Encoding key.
  * debian/earth3d.menu: 
    - Move from Apps/Science to Applications/Science/Geoscience.
  * debian/rules:
    - Don't ignore clean errors.
    - Use dh_prep instead of dh_clean -k.
  * debian/control: Add ${misc:Depends}.
  * debian/patches/10_fix_ftbfs.patch:
    - Fix gcc 4.6 FTBFS (LP: #770814), collect old patches.
  * debian/patches/20_png_library.patch:
    - Fix FTBFS with binutils-gold by linking to libpng,
      Thanks Ilya Barygin (Closes: #554314).
  * Acknowledge NMU (Closes: #417173).

 -- Aron Xu <aron@debian.org>  Mon, 27 Jun 2011 18:05:36 +0800

earth3d (1.0.5-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS with GCC 4.3 (Closes: #417173).

 -- Luk Claes <luk@debian.org>  Sun, 09 Mar 2008 11:14:10 +0000

earth3d (1.0.5-1) unstable; urgency=low

  * Convert to a non-native package, it was uploaded as a native package
    by mistake. (Closes: #385103)
  * Add KDE desktop entry to make the program show up in the KDE menu
    Edutainment/Science as well as the Debian menu Apps/Science.

 -- Petter Reinholdtsen <pere@debian.org>  Mon,  4 Sep 2006 08:17:56 +0200

earth3d (1.0.5) unstable; urgency=low

  * New upstream release.
    - Improve handling of scroll mouses.
  * Update standards-version to 3.7.2 (no changes needed).

 -- Petter Reinholdtsen <pere@debian.org>  Tue,  8 Aug 2006 19:13:59 +0200

earth3d (1.0.4-1) unstable; urgency=low

  * New upstream release.
    - New runtime options to select texture features.
    - Includes earth3d(1) manual page. (Closes: #327552)
    - Added client caching of geo data.
  * Drop 10_texture_compr.dpatch, as this is a runtime option now.
  * Add code to include menu icon.  Patch from Steffen Joeris.
    (Closes: #335884)

 -- Petter Reinholdtsen <pere@debian.org>  Tue,  1 Nov 2005 23:06:37 +0100

earth3d (1.0.3-1) unstable; urgency=low

  * New upstream release.
  * Undefine USE_GL_TEXTURE_COMPRESSION during built to get textures
    to show up with more X drivers.  (10_texture_compr.dpatch)
  * Removed patches applied upstream:
    - 10_gcc4.dpatch
    - 20_upstream_location.dpatch
    - 30_addnewline.dpatch
    - 40_badurl.dpatch

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 11 Sep 2005 00:00:54 +0200

earth3d (1.0.2-3) unstable; urgency=low

  * Added menu file.
  * Avoid crashing when a bad url is given to earth3d. (Closes: #323491)

 -- Petter Reinholdtsen <pere@debian.org>  Wed, 28 Aug 2005 16:25:50 +0200

earth3d (1.0.2-2) unstable; urgency=low

  * Add missing build-depend on dpatch.
  * Add trailing newline in network/urlTools.h to avoid compiler
    warning. (30_addnewline.dpatch)
  * Add the Debian GIS Project as an uploader.  This program is a GIS tool.

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 14 Aug 2005 09:13:15 +0200

earth3d (1.0.2-1) unstable; urgency=low

  * Initial release. (Closes: #317649)
  * Make QueuedMessage copy constructor take a const reference to get the
    code compiling using gcc 4.0. (10_gcc4.dpatch)
  * Add link to the sourceforge page in the README.
    (20_upstream_location.dpatch)

 -- Petter Reinholdtsen <pere@debian.org>  Tue,  2 Aug 2005 22:51:38 +0200
