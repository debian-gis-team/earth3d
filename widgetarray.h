#ifndef WIDGETARRAY_H
#define WIDGETARRAY_H
#include <qstring.h>
#include <qwidget.h>

struct WidgetArray {
  QString widgetname;
  QWidget *widget;

  WidgetArray() {
    widgetname = "";
    widget = NULL;
  };

  WidgetArray(const WidgetArray &w) {
    this->widgetname = w.widgetname;
    this->widget = w.widget;
  };

  WidgetArray(QString widgetname, QWidget *widget) {
    this->widgetname = widgetname;
    this->widget = widget;
  };
};

#endif
