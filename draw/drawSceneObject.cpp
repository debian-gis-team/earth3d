#include "drawSceneObject.h"

DrawSceneObject::DrawSceneObject(DrawScene *scene) {
  this->scene = scene;
  this->autoregister = true;

  scene->addDrawSceneObject(this);
}

DrawSceneObject::~DrawSceneObject() {
}

void DrawSceneObject::readXMLData(QDomNode n) {
  int lastvertex = 0, lasttexturecoord = 0;

  registerXMLNames(n);

  /* parse everything as children */
  QDomNode n3 = n.firstChild();
  while( !n3.isNull() ) {
    /* allow to add arbitrary objects */
    DrawSceneObject *object = scene->parseObject(n3);
    if (object) {
      addChild(object);
    }

    n3 = n3.nextSibling();
  }
}

void DrawSceneObject::registerXMLNames(QDomNode n) {
  /* register with its name */
  if (autoregister) {
    QString name = n.toElement().attribute("name");
    if (!name.isNull()) {
      scene->registerDrawSceneObjectName(name, this);
    }
  }
}

void DrawSceneObject::compile(std::vector<openglarray *> &targetlist, pbr_list *pbrlist) {
  /* compile all children */
  std::vector<DrawSceneObject *>::iterator i;
  for (i = children.begin(); i != children.end(); i++) {
    (*i)->compile(targetlist, pbrlist);
  }
}

void DrawSceneObject::mergeArray(std::vector<openglarray *> &list1, openglarray *list2array) {
  bool merged = false;

  std::vector<openglarray *>::iterator i2;
  for (i2 = list1.begin(); i2 != list1.end(); i2++) {
    openglarray *list1array = (openglarray *) *i2;
    if (list2array->texture == list1array->texture) {
      /* add vertices from list 2 into list1array, realloc the vertices arrays */
      
      Point3D *newvertexarray = new Point3D[list1array->vertexcount+list2array->vertexcount];
      memcpy(newvertexarray, list1array->vertexarray, list1array->vertexcount * sizeof(Point3D));
      memcpy(newvertexarray+list1array->vertexcount, list2array->vertexarray, list2array->vertexcount * sizeof(Point3D));
      
      Point2D *newtexcoordarray = new Point2D[list1array->vertexcount+list2array->vertexcount];
      memcpy(newtexcoordarray, list1array->texcoordarray, list1array->vertexcount * sizeof(Point2D));
      memcpy(newtexcoordarray+list1array->vertexcount, list2array->texcoordarray, list2array->vertexcount * sizeof(Point2D));
      
      delete[](list1array->vertexarray);
      list1array->vertexarray = newvertexarray;
      
      delete[](list1array->texcoordarray);
      list1array->texcoordarray = newtexcoordarray;
      
      list1array->vertexcount = list1array->vertexcount+list2array->vertexcount;

      merged = true;
    }
  }
  
  if (!merged) {
    /* add it */
    list1.push_back(list2array->clone());
  }
}

void DrawSceneObject::mergeLists(std::vector<openglarray *> &list1, std::vector<openglarray *> &list2) {
  /* insert all object of list 2 into list 1 */
  std::vector<openglarray *>::iterator i;
  for (i = list2.begin(); i != list2.end(); i++) {
    openglarray *list2array = (openglarray *) *i;

    mergeArray(list1, list2array);
  }
}

void DrawSceneObject::mergePBR(pbr_list *sourcelist, pbr_list *destlist) {
  for(int i=0; i<PBR_ANGLES_COUNT+2; i++) {
    pbr_list *source = sourcelist + i;
    pbr_list *dest = destlist+i;

    Point3D *vertexarray;
    Point3D *colorarray;
    
    vertexarray = new Point3D[source->vertexcount+dest->vertexcount];
    colorarray = new Point3D[source->vertexcount+dest->vertexcount];
    
    memcpy(vertexarray, dest->vertexarray, dest->vertexcount*sizeof(Point3D));
    memcpy(vertexarray+dest->vertexcount, source->vertexarray, source->vertexcount*sizeof(Point3D));
    
    memcpy(colorarray, dest->colorarray, dest->vertexcount*sizeof(Point3D));
    memcpy(colorarray+dest->vertexcount, source->colorarray, source->vertexcount*sizeof(Point3D));
    
    delete[](dest->vertexarray);
    dest->vertexarray = vertexarray;
    
    delete[](dest->colorarray);
    dest->colorarray = colorarray;
    
    dest->vertexcount += source->vertexcount;
  }
}

void DrawSceneObject::deleteList(std::vector<openglarray *> &list) {
  std::vector<openglarray *>::iterator i;
  for (i = list.begin(); i != list.end(); i++) {
    delete[]((*i)->vertexarray);
    delete[]((*i)->texcoordarray);
    delete(*i);
  }
}

void DrawSceneObject::addChild(DrawSceneObject *child) {
  children.push_back(child);
}

void DrawSceneObject::deleteChildren() {
  /* compile all children */
  std::vector<DrawSceneObject *>::iterator i;
  for (i = children.begin(); i != children.end(); i++) {
    (*i)->deleteChildren();
    scene->deregisterDrawSceneObjectName(*i);
    scene->removeDrawSceneObject(*i);
    delete(*i);
  }  
  children.clear();
}

void DrawSceneObject::erasePBRList(pbr_list *pbrlist) {
  if (!pbrlist) return;

  /* erase pbrlist */
  for(int p=0; p<PBR_ANGLES_COUNT+2; p++) {
    pbrlist[p].vertexcount = 0;
    delete[](pbrlist[p].vertexarray);
    delete[](pbrlist[p].colorarray);
    pbrlist[p].vertexarray = new Point3D[1];
    pbrlist[p].colorarray = new Point3D[1];
  }
}
