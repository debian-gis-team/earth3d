#ifndef DRAWSCENEOBJECTTRIANGLE_H
#define DRAWSCENEOBJECTTRIANGLE_H

#include "drawSceneObject.h"
#include "drawSceneObjectTexture.h"
#include <qdom.h>
#include "point3d.h"

class DrawSceneObjectTriangle : public DrawSceneObject {
  Point3D vertexarray[3];
  Point2D texcoordarray[3];
  QString texturename;

  openglarray triangleopenglarray;

 public:
  DrawSceneObjectTriangle(DrawScene *scene);
  virtual ~DrawSceneObjectTriangle();

  /** Read its attributes from the XML document */
  virtual void readXMLData(QDomNode n);

  /** Compiles this object and all of it children into the vector */
  virtual void compile(std::vector<openglarray *> &targetlist, pbr_list *pbrlist);
};

#endif
