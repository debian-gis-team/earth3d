#include "drawSceneObjectTransform.h"

DrawSceneObjectTransform::DrawSceneObjectTransform(DrawScene *scene)
  : DrawSceneObject(scene) {
}

DrawSceneObjectTransform::~DrawSceneObjectTransform() {
}

void DrawSceneObjectTransform::compile(std::vector<openglarray *> &targetlist, pbr_list *pbrlist) {
  /* compile all children into a special vector */
  std::vector<openglarray *> childrendata;
  DrawSceneObject::compile(childrendata, pbrlist);

  /* modifiy data in this vector */
  transform(childrendata);

  /* modify points in pbr list */
  if (pbrlist) transformPBR(pbrlist);

  /* add data to targetlist */
  mergeLists(targetlist, childrendata);

  /* delete everything in childrendata */
  deleteList(childrendata);
}

void DrawSceneObjectTransform::transform(std::vector<openglarray *> &targetlist) {
  std::vector<openglarray *>::iterator i;
  for (i = targetlist.begin(); i != targetlist.end(); i++) {
    openglarray *element = (openglarray *) *i;

    transformElement(element);
  }
}

void DrawSceneObjectTransform::transformPBR(pbr_list *pbrlist) {
  for(int p=0; p<PBR_ANGLES_COUNT+2; p++) {
    for(int i=0; i<pbrlist[p].vertexcount; i++) {
      pbrlist[p].vertexarray[i] = transformPoint3D(pbrlist[p].vertexarray[i]);
    }
  }
}

void DrawSceneObjectTransform::transformElement(openglarray *element) {
  for(int v=0; v<element->vertexcount; v++) {
    element->vertexarray[v]=transformPoint3D(element->vertexarray[v]);
  }
}
