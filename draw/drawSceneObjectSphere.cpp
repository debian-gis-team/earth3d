#include "drawSceneObjectSphere.h"
#include <assert.h>

DrawSceneObjectSphere::DrawSceneObjectSphere(DrawScene *scene) 
  : DrawSceneObject(scene) {
  stacks = 0;
  slices = 0;
  triangleopenglarray.vertexcount = 0;
  triangleopenglarray.vertexarray = new Point3D[3];
  triangleopenglarray.texcoordarray = new Point2D[3];
}

DrawSceneObjectSphere::~DrawSceneObjectSphere() {
  delete(triangleopenglarray.vertexarray);
  delete(triangleopenglarray.texcoordarray);
}

void DrawSceneObjectSphere::readXMLData(QDomNode n) {
  registerXMLNames(n);

  int lastvertex = 0, lasttexturecoord = 0;

  /* n contains the sphere node */
  QDomElement sphereElement = n.toElement();

  centervertex.x = sphereElement.attribute("x","0").toDouble();
  centervertex.y = sphereElement.attribute("y","0").toDouble();
  centervertex.z = sphereElement.attribute("z","0").toDouble();
  radius = sphereElement.attribute("radius","1").toDouble();
  slices = sphereElement.attribute("slices","10").toInt();
  stacks = sphereElement.attribute("stacks","10").toInt();

  QDomNode n3 = n.firstChild();
  while( !n3.isNull() ) {
    /* get a texture */
    if (n3.isElement() && (n3.toElement().tagName() == QString("usetex"))) {
      texturename = n3.toElement().attribute("name");
    }
    else {
      /* allow to add arbitrary objects */
      DrawSceneObject *object = scene->parseObject(n3);
      if (object) {
	addChild(object);
      }
    }

    n3 = n3.nextSibling();
  }
}

void DrawSceneObjectSphere::compile(std::vector<openglarray *> &targetlist, pbr_list *pbrlist) {
  /* compile this sphere into triangles */
  DrawSceneObjectTexture *texture = (DrawSceneObjectTexture *) scene->getRegisteredDrawSceneObject(texturename);

  triangleopenglarray.texture = texture;
  triangleopenglarray.vertexcount = stacks*slices*3*2;
  delete(triangleopenglarray.vertexarray);
  delete(triangleopenglarray.texcoordarray);
  triangleopenglarray.vertexarray = new Point3D[triangleopenglarray.vertexcount];
  triangleopenglarray.texcoordarray = new Point2D[triangleopenglarray.vertexcount];

  int vertexcount = 0;
  for(int st=0; st<stacks; st++) {
    for(int sl=0; sl<slices; sl++) {
      if (st==0) {
	triangleopenglarray.vertexarray[vertexcount] = centervertex+sphere.getPoint(Point2D(double(sl)/double(slices), double(st)/double(stacks)), radius);
	triangleopenglarray.texcoordarray[vertexcount++] = Point2D(double(sl)/double(slices), 1.-double(st)/double(stacks));
	triangleopenglarray.vertexarray[vertexcount] = centervertex+sphere.getPoint(Point2D(double(sl+1)/double(slices), double(st+1)/double(stacks)), radius);
	triangleopenglarray.texcoordarray[vertexcount++] = Point2D(double(sl+1)/double(slices), 1.-double(st+1)/double(stacks));
	triangleopenglarray.vertexarray[vertexcount] = centervertex+sphere.getPoint(Point2D(double(sl)/double(slices), double(st+1)/double(stacks)), radius);
	triangleopenglarray.texcoordarray[vertexcount++] = Point2D(double(sl)/double(slices), 1.-double(st+1)/double(stacks));
      }
      else if (st==stacks-1) {
	triangleopenglarray.vertexarray[vertexcount] = centervertex+sphere.getPoint(Point2D(double(sl)/double(slices), double(st)/double(stacks)), radius);
	triangleopenglarray.texcoordarray[vertexcount++] = Point2D(double(sl)/double(slices), 1.-double(st)/double(stacks));
	triangleopenglarray.vertexarray[vertexcount] = centervertex+sphere.getPoint(Point2D(double(sl+1)/double(slices), double(st)/double(stacks)), radius);
	triangleopenglarray.texcoordarray[vertexcount++] = Point2D(double(sl+1)/double(slices), 1.-double(st)/double(stacks));
	triangleopenglarray.vertexarray[vertexcount] = centervertex+sphere.getPoint(Point2D(double(sl)/double(slices), double(st+1)/double(stacks)), radius);
	triangleopenglarray.texcoordarray[vertexcount++] = Point2D(double(sl)/double(slices), 1.-double(st+1)/double(stacks));
      }
      else {
	triangleopenglarray.vertexarray[vertexcount] = centervertex+sphere.getPoint(Point2D(double(sl)/double(slices), double(st)/double(stacks)), radius);
	triangleopenglarray.texcoordarray[vertexcount++] = Point2D(double(sl)/double(slices), 1.-double(st)/double(stacks));
	triangleopenglarray.vertexarray[vertexcount] = centervertex+sphere.getPoint(Point2D(double(sl+1)/double(slices), double(st)/double(stacks)), radius);
	triangleopenglarray.texcoordarray[vertexcount++] = Point2D(double(sl+1)/double(slices), 1.-double(st)/double(stacks));
	triangleopenglarray.vertexarray[vertexcount] = centervertex+sphere.getPoint(Point2D(double(sl)/double(slices), double(st+1)/double(stacks)), radius);
	triangleopenglarray.texcoordarray[vertexcount++] = Point2D(double(sl)/double(slices), 1.-double(st+1)/double(stacks));
	
	triangleopenglarray.vertexarray[vertexcount] = centervertex+sphere.getPoint(Point2D(double(sl+1)/double(slices), double(st)/double(stacks)), radius);
	triangleopenglarray.texcoordarray[vertexcount++] = Point2D(double(sl+1)/double(slices), 1.-double(st)/double(stacks));
	triangleopenglarray.vertexarray[vertexcount] = centervertex+sphere.getPoint(Point2D(double(sl+1)/double(slices), double(st+1)/double(stacks)), radius);
	triangleopenglarray.texcoordarray[vertexcount++] = Point2D(double(sl+1)/double(slices), 1.-double(st+1)/double(stacks));
	triangleopenglarray.vertexarray[vertexcount] = centervertex+sphere.getPoint(Point2D(double(sl)/double(slices), double(st+1)/double(stacks)), radius);
	triangleopenglarray.texcoordarray[vertexcount++] = Point2D(double(sl)/double(slices), 1.-double(st+1)/double(stacks));
      }

      assert(vertexcount<=stacks*slices*3*2);
    }
  }

  /* compile all children */
  DrawSceneObject::compile(targetlist, pbrlist);

  /* merge our triangles to the list */
  mergeArray(targetlist, &triangleopenglarray);
}
