#ifndef DRAWSCENEOBJECTTRANSFORM_H
#define DRAWSCENEOBJECTTRANSFORM_H

#include "drawSceneObject.h"

class DrawSceneObjectTransform : public DrawSceneObject {
 public:
  DrawSceneObjectTransform(DrawScene *scene);
  virtual ~DrawSceneObjectTransform();

  /** Read its attributes from the XML document */
  virtual void readXMLData(QDomNode n) = 0;

  /** Modify data */
  virtual void transform(std::vector<openglarray *> &targetlist);

  /** Modify PBR data */
  virtual void transformPBR(pbr_list *pbrlist);

  /** Modify single entry */
  virtual void transformElement(openglarray *element);

  /** Modify single point */
  virtual Point3D transformPoint3D(Point3D &p) = 0;

  /** Compiles this object and all of it children into the vector */
  virtual void compile(std::vector<openglarray *> &targetlist, pbr_list *pbrlist);
};

#endif
