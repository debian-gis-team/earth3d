#ifndef TREEDRAWPOIOBJECT_H
#define TREEDRAWPOIOBJECT_H
#include "winconf.h"
#include "point3d.h"
#include <qgl.h>

class TreeDrawPOI;

class TreeDrawPOIObject {
 public:
  virtual void draw(Point3D *viewer, bool viewculling, QGLWidget *widget, TreeDrawPOI *poi) = 0;
  virtual bool mouseDoubleClickEvent(Point3D *viewer, bool viewculling, QGLWidget *widget, Point3D direction, QMouseEvent *e, TreeDrawPOI *poi) = 0;
};

#endif
