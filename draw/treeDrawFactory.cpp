#include "treeDrawFactory.h"
#include <qtextstream.h>
#include <qdom.h>
#include "treeDraw.h"
#include "treeDrawSphere.h"
#include "mapTileTree.h"
#include "textureTreeNodeCore.h"
#include "heightfieldTreeNodeCore.h"
#include "treeDrawPOI.h"
#include "drawScene.h"
#include "formview.h"
#include "moveToPositionEvent.h"

using namespace std;

TreeDrawFactory::TreeDrawFactory(GraphicsObjectsContainer *goc, ConnectNetworkService *cns, GLTestWidget *glwidget) {
  this->goc = goc;
  this->cns = cns;
  this->glwidget = glwidget;
  this->ignoreDraw = (Draw *) 1;
}

TreeDrawFactory::~TreeDrawFactory() {
}

void TreeDrawFactory::tagNavigation(QDomNode navigationNode, int msgid, const char *sender, ConnectNetworkService *cns) {
  QDomNode navNode = navigationNode.firstChild();

  while(!navNode.isNull()) {

    /* requestpos */

    if (navNode.isElement() && navNode.toElement().tagName() == QString("requestpos")) {
      /* agent requests user position, send answer */
      QString xmlrequest = QString("<?xml version=\"1.0\"?><get msgid=\"") + QString::number(msgid) + "\"><agent><navigationpos>";

      /* viewer */
      Point3D pos = glwidget->getNavigator()->getViewer();
      xmlrequest += "<viewer x=\""+QString().setNum(pos.x)+"\" "+
	"y=\""+QString().setNum(pos.y)+"\" "+
	"z=\""+QString().setNum(pos.z)+"\" />";

      /* direction */
      pos = glwidget->getNavigator()->getDirection();
      xmlrequest += "<direction x=\""+QString().setNum(pos.x)+"\" "+
	"y=\""+QString().setNum(pos.y)+"\" "+
	"z=\""+QString().setNum(pos.z)+"\" />";

      /* up */
      pos = glwidget->getNavigator()->getUp();
      xmlrequest += "<up x=\""+QString().setNum(pos.x)+"\" "+
	"y=\""+QString().setNum(pos.y)+"\" "+
	"z=\""+QString().setNum(pos.z)+"\" />";

      /* surface coordinates */
      Point2D pos2d = sphere.inverse(glwidget->getNavigator()->getViewer());
      xmlrequest += "<surface x=\""+QString().setNum(pos2d.x)+"\" "+
	"y=\""+QString().setNum(pos2d.y)+"\" "+
	"z=\""+QString().setNum(glwidget->getNavigator()->getViewer().length()-1.)+"\" />";

      xmlrequest += "</navigationpos></agent></get>";
      cns->send(sender, xmlrequest.latin1());
    }

    /* setviewer */

    if (navNode.isElement() && navNode.toElement().tagName() == QString("setviewer")) {
      /* agent sets user position */
      QDomElement posElement = navNode.toElement();

      /* viewer */
      Point3D pos = Point3D(posElement.attribute("x", "0").toDouble(),
			    posElement.attribute("y", "0").toDouble(), 
			    posElement.attribute("z", "0").toDouble());

      if (posElement.attribute("offset", "false") == QString("true")) {
	pos = glwidget->getNavigator()->getViewer() + pos;
      }

      glwidget->getNavigator()->setViewer(pos);
    }

    /* setdirection */

    if (navNode.isElement() && navNode.toElement().tagName() == QString("setdirection")) {
      /* agent sets user position */
      QDomElement posElement = navNode.toElement();

      /* viewer */
      Point3D pos = Point3D(posElement.attribute("x", "0").toDouble(),
			    posElement.attribute("y", "0").toDouble(), 
			    posElement.attribute("z", "0").toDouble());

      if (posElement.attribute("offset", "false") == QString("true")) {
	pos = glwidget->getNavigator()->getDirection() + pos;
      }

      glwidget->getNavigator()->setDirection(pos);
    }

    /* setup */

    if (navNode.isElement() && navNode.toElement().tagName() == QString("setup")) {
      /* agent sets user position */
      QDomElement posElement = navNode.toElement();

      /* viewer */
      Point3D pos = Point3D(posElement.attribute("x", "0").toDouble(),
			    posElement.attribute("y", "0").toDouble(), 
			    posElement.attribute("z", "0").toDouble());

      if (posElement.attribute("offset", "false") == QString("true")) {
	pos = glwidget->getNavigator()->getUp() + pos;
      }

      glwidget->getNavigator()->setUp(pos);
    }

    navNode = navNode.nextSibling();
  }
}

Draw *TreeDrawFactory::tagGeometryGpsMark(QDomNode geometryNode, Draw *newDraw) {
  /* fill in the data */
  QDomNode n3 = geometryNode.firstChild();
  while( !n3.isNull() ) {
    if (n3.isElement() && (n3.toElement().tagName() == QString("mark"))) {
      Point3D mark, dir, up;
      QString markName;

      QDomElement markElement = n3.toElement();
      markName = markElement.attribute("name");

      /* get the position */
      mark.x = atof(markElement.attribute("x").latin1());
      mark.y = atof(markElement.attribute("y").latin1());
      mark.z = atof(markElement.attribute("z").latin1());

      /* get the view direction */
      dir.x = atof(markElement.attribute("dirx","0").latin1());
      dir.y = atof(markElement.attribute("diry","0").latin1());
      dir.z = atof(markElement.attribute("dirz","0").latin1());

      /* get the up direction */
      up.x = atof(markElement.attribute("upx","0").latin1());
      up.y = atof(markElement.attribute("upy","1").latin1());
      up.z = atof(markElement.attribute("upz","0").latin1());

      /* create event */
      qApp->postEvent(formview, new MoveToPositionEvent(markName, mark, dir, up));
    }
      
    n3 = n3.nextSibling();
  }

  return(NULL);
}

void TreeDrawFactory::tagGeometry(QDomNode geometryNode, int msgid, const char *sender, ConnectNetworkService *cns) {
  QDomElement geometryElement = geometryNode.toElement();

  /* Construct a Draw object from this geometry object */
  QString type = geometryElement.attribute("type");
  QString name = geometryElement.attribute("name", "no name");

  /* see it this object was already created and should be updated */

  bool gocAdd = true;
  Draw *newDraw = NULL;
  newDraw = gocIDs[msgid];

  if (newDraw==ignoreDraw) { // ignore this geometry object, it was already unloaded
  }
  else {
    if (!newDraw) {
      if (geometryElement.hasAttribute("identifier")) {
	QString identifier = geometryElement.attribute("identifier");
	newDraw = goc->findListElementByIdentifier(identifier);
      }
    }
    if (newDraw) gocAdd = false;

    if (type == QString("maptree")) {
      newDraw = tagGeometryMapTree(geometryNode, newDraw, gocAdd);
    }
    if (type == QString("poi")) {
      newDraw = TreeDrawPOI::tagGeometry(geometryNode, newDraw);
    }
    if (type == QString("scene")) {
      newDraw = DrawScene::tagGeometry(geometryNode, newDraw);
    }
    if (type == QString("gpsmark")) {
      newDraw = tagGeometryGpsMark(geometryNode, newDraw);
    }
	  
    /* Insert Draw into GraphicsObjectsContainer */
    if (newDraw) {
      newDraw->setName(name);
      if (gocAdd) {
	/* insert type identifier */
	if (geometryElement.hasAttribute("identifier")) {
	  QString *s = new QString(geometryElement.attribute("identifier"));
	  goc->add(newDraw, s);
	  delete(s);
	}
	else {
	  goc->add(newDraw, NULL);
	}
      }
      /* insert object into ID list if it has an ID */
      if (msgid>=0) gocIDs[msgid] = newDraw;

      newDraw = NULL;
    }
  }
}


Draw *TreeDrawFactory::tagGeometryMapTree(QDomNode geometryNode, Draw *newDraw, bool &gocAdd) {
  QDomElement geometryElement = geometryNode.toElement();
  QString base = geometryElement.attribute("base", QString("sphere"));
  int layer = geometryElement.attribute("layer", "1").toInt();
  QString atmosphere = geometryElement.attribute("atmosphere", QString("false"));

  /* TreeDraw objects cannot be updated for now */
  goc->remove(newDraw); // remove old object with same ID
  delete(newDraw); // delete it
  newDraw = NULL;
  gocAdd = true; // add this object as new

  TreeDraw *newTreeDraw = NULL;
  MapTileTree *newMapTileTree = new MapTileTree();
  MapTileTreeNode *newRootNode = newMapTileTree->getRootNode();

  if (newRootNode) {
    /* fill new created object with cores */

    /* search for texture or heightfield tag */
    QDomNode n3 = geometryNode.firstChild();
    while( !n3.isNull() ) {
      if (n3.isElement() && (n3.toElement().tagName() == QString("texture") || n3.toElement().tagName() == QString("heightfield"))) {
	int coreIndex = 0;
	MapTileTreeNodeCore *newCore = NULL;
	if (n3.toElement().tagName() == QString("texture")) {
	  coreIndex = 0;
	  newCore = new TextureTreeNodeCore();
	}
	if (n3.toElement().tagName() == QString("heightfield")) {
	  coreIndex = 1;
	  newCore = new HeightfieldTreeNodeCore();
	}

	/* search for connections tag */
	QDomNode n4 = n3.firstChild();
	while( !n4.isNull() ) {
	  if (n4.isElement() && n4.toElement().tagName() == QString("connections")) {
	    QString output;
	    QTextStream ts(&output, IO_WriteOnly);
	    ts << n4;
	    cout << "TreeDrawFactory" << output.latin1() << " END " << endl;

	    /* set the connection in the core */
	    newCore->setRequestID(output.latin1());
	  }
		  
	  n4 = n4.nextSibling();
	}

	/* insert the core */
	newRootNode->setCore(coreIndex, newCore);
      }

      n3 = n3.nextSibling();
    }
  }

  newTreeDraw = new TreeDrawSphere(newMapTileTree, cns, base.latin1(), atmosphere==QString("true"));

  return(newTreeDraw);
}

void TreeDrawFactory::dataReceived(const char *response, int size, const char *sender, int countParts, struct part_t *parts, void *userdata, ConnectNetworkService *cns, StopableDownload *download) {
  printf("Geometry data received.\n");

  if (cns==NULL) {
    cns=this->cns;
  }

  QDomDocument doc;
  if (doc.setContent(QString(response))) {
    /* search for response tag */
    QDomNode n = doc.documentElement();
    if (!n.isNull() && n.isElement() && n.toElement().tagName() == QString("response")) {

      // use msgid as gocID to reference it later

      int msgid = atoi(n.toElement().attribute("msgid", "-1"));

      /* search for geometry tag */
      QDomNode n2 = n.firstChild();
      while( !n2.isNull() ) {

	if (n2.isElement() && n2.toElement().tagName() == QString("navigation")) {
	  /* a navigation tag requests or sets navigational data */
	  tagNavigation(n2, msgid, sender, cns);
	}

	if (n2.isElement() && n2.toElement().tagName() == QString("geometry")) {
	  /* a geometry tag displays some 3d data */
	  tagGeometry(n2, msgid, sender, cns);
	}
	n2 = n2.nextSibling();
      }
    }
  }

  printf("Geometry data received finished.\n");
}

void TreeDrawFactory::remove(Draw *draw) {
  std::map<long, Draw *>::iterator gociterator;
  for(gociterator=gocIDs.begin(); gociterator!=gocIDs.end(); gociterator++) {
    if ((*gociterator).second==draw) {
      gocIDs[(*gociterator).first]=ignoreDraw;
      break;
    }
  }
}
