#include "treeDraw.h"

TreeDraw::TreeDraw(MapTileTree *mtt) : Draw() {
  this->mtt = mtt;
}

TreeDraw::~TreeDraw() {
  if (mtt) {
    delete(mtt);
    mtt = NULL;
  }
}


