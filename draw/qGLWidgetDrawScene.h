#ifndef QGLWIDGETDRAWSCENE_H
#define QGLWIDGETDRAWSCENE_H

#include "winconf.h"
#include <qgl.h>
#include <vector>
#include "drawScene.h"
#include <qmutex.h>
#include <qthread.h>
#include <qimage.h>
#include <qtimer.h>

struct ColoredPoint3D {
  Point3D p;
  Point3D color;

  ColoredPoint3D(Point3D p, Point3D color) {
    this->p = p;
    this->color = color;
  };
};

class QGLWidgetDrawScene : public QGLWidget, public QThread {
  Q_OBJECT

  GLboolean cullface;
  firstLevelOpenGLArray *flcurrent;

  std::vector<firstLevelOpenGLArray *> firstlevelcompilelist;

  int pbrAnglesCount, pbrUseAnglesAround;
  float pbrAnglesElevation;
  int currentangle;
  Point3D backgroundcolor;

  double   proj[16];	// This will hold our projection matrix
  double   modl[16];	// This will hold our modelview matrix
  GLint viewport[4];

  QMutex drawMutex;
  bool drawn;

  QImage lastimage;

  DrawScene *scene;

  QTimer *timer;

 protected:
  virtual void initializeGL();
  virtual void resizeGL(int width, int height);
  virtual void paintGL();


  QImage render(Point3D backgroundcolor, int angle);
  void readPoints(QImage &image1, QImage &image2, std::vector<ColoredPoint3D> &tmppoints);

  std::vector<firstLevelOpenGLArray *>::iterator fi;

 public slots:
  virtual void nextFrame();

 public:
  QGLWidgetDrawScene(DrawScene *scene);
  virtual ~QGLWidgetDrawScene();

#ifdef WIN32
  void start ( Priority priority = InheritPriority );
#endif
  virtual void run();

  void renderPBRData(firstLevelOpenGLArray *flobject, int pbrAnglesCount, float pbrAnglesElevation, int pbrUseAnglesAround);
  void setPBRList(std::vector<firstLevelOpenGLArray *> firstlevelcompilelist);
  int depthWidth, depthHeight;
  float *depthbuffer;

  bool running;
};

#endif
