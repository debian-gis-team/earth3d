#include "drawSceneObjectScale.h"

DrawSceneObjectScale::DrawSceneObjectScale(DrawScene *scene)
  : DrawSceneObjectTransform(scene) {
}

DrawSceneObjectScale::~DrawSceneObjectScale() {
}

void DrawSceneObjectScale::readXMLData(QDomNode n) {
  /* n is the texture node */
  scale.x = atof(n.toElement().attribute("x"));
  scale.y = atof(n.toElement().attribute("y"));
  scale.z = atof(n.toElement().attribute("z"));

  /* parse children */
  DrawSceneObject::readXMLData(n);
}

Point3D DrawSceneObjectScale::transformPoint3D(Point3D &p) {
  return(Point3D(p.x*scale.x, p.y*scale.y, p.z*scale.z));
}
