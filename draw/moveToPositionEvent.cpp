#include "moveToPositionEvent.h"

MoveToPositionEvent::MoveToPositionEvent(QString name, Point3D mark, Point3D dir, Point3D up)
  : QEvent((QEvent::Type) 1004) {
  this->name = name;
  this->mark = mark;
  this->dir = dir;
  this->up = up;
}

MoveToPositionEvent::~MoveToPositionEvent() {
}

QString MoveToPositionEvent::getName() {
  return(name);
}

Point3D MoveToPositionEvent::getMark() {
  return(mark);
}

Point3D MoveToPositionEvent::getDir() {
  return(dir);
}

Point3D MoveToPositionEvent::getUp() {
  return(up);
}
