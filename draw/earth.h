#ifndef EARTH_H
#define EARTH_H

// real earth radius is  6,378.14 km
#define REALEARTHRADIUS 6378.14

#ifdef DAVE
#define EARTHRADIUS 188000
#else
#define EARTHRADIUS 378000
#endif

#endif
