#ifndef TREEDRAWFACTORY_H
#define TREEDRAWFACTORY_H
#include "dataReceivedListener.h"
#include "graphicsObjectsContainer.h"
#include "connectNetworkService.h"
#include <map>
#include "draw.h"
#include "gltest.h"
#include "geometry2d3dSphere.h"
#include <qdom.h>

/** This class builds Draw objects from a downloaded description. In the dataReceived method
 *  the type determined and the correct class is called to build an object from the data.
 *
 *  To add some new visual object type, add a protected method like tagGeometryPOI and insert
 *  it into tagGeomtry. The new object must be inherited from the Draw class.
 */
class TreeDrawFactory : public DataReceivedListener {
  /** The list of visible objects */
  GraphicsObjectsContainer *goc;
  /** The network connection to retrieve objects */
  ConnectNetworkService *cns;
  /** The main OpenGL widget */
  GLTestWidget *glwidget;

  Geometry2D3DSphere sphere;

  /** Map every graphics object to a unique number to reference it later from the
   *  agent system.
   */
  Draw *ignoreDraw;

  std::map<long, Draw *> gocIDs;
 protected:
  void tagNavigation(QDomNode navigationNode, int msgid, const char *sender, ConnectNetworkService *cns);
  void tagGeometry(QDomNode geometryNode, int msgid, const char *sender, ConnectNetworkService *cns);
  Draw *tagGeometryMapTree(QDomNode geometryNode, Draw *newDraw, bool &gocAdd);
  Draw *tagGeometryGpsMark(QDomNode geometryNode, Draw *newDraw);

 public:
  TreeDrawFactory(GraphicsObjectsContainer *goc, ConnectNetworkService *cns, GLTestWidget *glwidget);
  virtual ~TreeDrawFactory();

  /** Parses the received data to determine the type and build a Draw object. The new object is added to the list
   *  of visible objects and will receive draw events. In this method updates to existing objects are also handled.
   */
  virtual void dataReceived(const char *response, int size, const char *sender, int countParts, struct part_t *parts, void *userdata, ConnectNetworkService *cns, StopableDownload *download);

  /* remove all references to this Draw object */
  virtual void remove(Draw *draw);
};

#endif
