#ifndef TREEDRAW_H
#define TREEDRAW_H
#include "mapTileTree.h"
#include "point3d.h"
#include "draw.h"

class TreeDraw : public Draw {
 protected:
  /** The tree this TreeDraw object should draw. */
  MapTileTree *mtt;

 public:
  TreeDraw(MapTileTree *mtt);
  virtual ~TreeDraw();
  virtual void draw(Point3D *viewer, bool viewculling, QGLWidget *widget, Point3D direction, int phase) = 0;
};

#endif
