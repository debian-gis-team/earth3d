#include "drawSceneObjectTranslate.h"

DrawSceneObjectTranslate::DrawSceneObjectTranslate(DrawScene *scene)
  : DrawSceneObjectTransform(scene) {
}

DrawSceneObjectTranslate::~DrawSceneObjectTranslate() {
}

void DrawSceneObjectTranslate::readXMLData(QDomNode n) {
  /* n is the texture node */
  translation.x = atof(n.toElement().attribute("x"));
  translation.y = atof(n.toElement().attribute("y"));
  translation.z = atof(n.toElement().attribute("z"));

  /* parse children */
  DrawSceneObject::readXMLData(n);
}

Point3D DrawSceneObjectTranslate::transformPoint3D(Point3D &p) {
  return(p+translation);
}
