#ifndef DRAWSCENE_H
#define DRAWSCENE_H
#include "draw.h"
#include <qdom.h>
#include <vector>
#include <qmap.h>
#include <qmutex.h>
#include "point3d.h"
#include "point2d.h"
#include "geometry2d3dSphere.h"

#define PBR_ANGLES_COUNT 8
#define PBR_ANGLES_ELEVATION 0
#define PBR_USE_ANGLES_AROUND 0
#define PBR_IMAGE_SIZE 128

class DrawSceneObject;
class DrawSceneObjectTexture;
class DrawSceneObjectGroup;
class QGLWidgetDrawScene;

class openglarray {
 public:
#ifdef WIN32
  static const int magic=1234;
#endif
  DrawSceneObjectTexture *texture;

  int vertexcount;
  Point3D *vertexarray;
  Point2D *texcoordarray;

  openglarray *clone();
};

class pbr_list {
 public:
  int vertexcount;
  Point3D *vertexarray;
  Point3D *colorarray;

  pbr_list() {
    vertexcount = 0;
    vertexarray = new Point3D[1];
    colorarray = new Point3D[1];
  };

  ~pbr_list() {
    if (vertexarray) delete[](vertexarray);
    if (colorarray) delete[](colorarray);
  };
};

/** For every first level object one list is created */
class firstLevelOpenGLArray {
 public:
  std::vector<openglarray *> openglarrayList;
  pbr_list pbrlist[PBR_ANGLES_COUNT+2]; // around + top + bottom

  /* for the group list */
  DrawSceneObjectGroup *group;

  /* bounding sphere */
  Point3D bs_center;
  float bs_radius;

  firstLevelOpenGLArray(std::vector<openglarray *> &openglarrayList);
  ~firstLevelOpenGLArray() {
  };
};

class DrawScene : public Draw {
 protected:
  /* point based rendering */
  QGLWidgetDrawScene *glwidget;
  bool pointbasedrendering_generated;
  int pbrphase;
  pbr_list grouppbrlist[PBR_ANGLES_COUNT+2];

  bool usePBR;

  void eraseGroupPBRList();

  /** contains the group objects for pbr */
  std::vector<DrawSceneObjectGroup *> groupList;

  /** a list that contains all vertex arrays that must be drawn for all registered groups */
  std::vector<firstLevelOpenGLArray *> groupcompilelist;

  /** contains one list of vertices for every texture */
  std::vector<openglarray *> openglarrayList;

  /** contains the root level of the scene graph */
  std::vector<DrawSceneObject *> rootList;

  /** global namespace for updating objects */
  QMap<QString, DrawSceneObject *> drawSceneObjectNamespaceList;

  /** creates vertex and texture arrays from the scenegraph */
  void compile();

  /** a list containing all objects of the scene. It is used for the destructor */
  std::vector<DrawSceneObject *> fullList;

  /** a list that contains all vertex arrays that must be drawn */
  std::vector<firstLevelOpenGLArray *> firstlevelcompilelist;

  int unfinishedDownloads;

  bool downloadsFinished;
  bool parseFinished;

  QMutex compileMutex;
  QMutex changeMutex;

  Geometry2D3DSphere sphere;

  void createBoundingSphere(std::vector<openglarray *> &compilelist, Point3D &bs_center, float &bs_radius);

  /** calculates the bounding spheres for all objects in the list */
  void createBoundingSpheres(std::vector<firstLevelOpenGLArray *> &firstlevelcompilelist);

  void eraseFirstLevelArray(std::vector<firstLevelOpenGLArray *> &firstlevelcompilelist);

  int drawphase;

 public:
  DrawScene(int drawphase=1, bool usrPBR=true);
  virtual ~DrawScene();

  static Draw *tagGeometry(QDomNode geometryNode, Draw *newDraw);

  virtual void draw(Point3D *viewer, bool viewculling, QGLWidget *widget, Point3D direction, int phase);

  /** removes all scene data */
  void clear();

  /** parses the XML document to create the scene graph */
  void fillSceneData(QDomNode n);

  /** register a nodes name in the global namespace */
  void registerDrawSceneObjectName(QString name, DrawSceneObject *object);

  /** deregister a nodes name from the global namespace */
  void deregisterDrawSceneObjectName(DrawSceneObject *object);

  /** get a registered node from global namespace */
  DrawSceneObject *getRegisteredDrawSceneObject(QString name);

  /** adds an object to the list of all objects of this scene. It is used for deletion in the destructor
   *  since the objects can have multiple references in the tree (but one has to prevent circular references).
   */
  void addDrawSceneObject(DrawSceneObject *object);

  void removeDrawSceneObject(DrawSceneObject *object);

  /** Creates a new object based on the content of the node.
   *  @returns a reference to the created object or NULL if it was not parsable
   */
  DrawSceneObject *parseObject(QDomNode n);

  /** Adds an unfinished download to the list. The scene will not be compiled and
   *  displayed until all downloads are finished.
   */
  void addDownload();

  /** Removed an unfinished download from the list. The scene will not be compiled and
   *  displayed until all downloads are finished.
   */
  void removeDownload();

  void registerGroupObjectForPBR(DrawSceneObjectGroup *object);

  void setFinishedPBR();
};

#endif
