#include "drawSceneObjectTriangle.h"

DrawSceneObjectTriangle::DrawSceneObjectTriangle(DrawScene *scene) 
  : DrawSceneObject(scene) {

  triangleopenglarray.vertexcount = 3;
  triangleopenglarray.vertexarray = new Point3D[3];
  triangleopenglarray.texcoordarray = new Point2D[3];
}

DrawSceneObjectTriangle::~DrawSceneObjectTriangle() {
  delete(triangleopenglarray.vertexarray);
  delete(triangleopenglarray.texcoordarray);
}

void DrawSceneObjectTriangle::readXMLData(QDomNode n) {
  int lastvertex = 0, lasttexturecoord = 0;

  /* n contains the triangle node */
  QDomNode n3 = n.firstChild();
  while( !n3.isNull() ) {
    /* get a texture */
    if (n3.isElement() && (n3.toElement().tagName() == QString("usetex"))) {
      texturename = n3.toElement().attribute("name");
    }
    else {
      /* get 3 vertices */
      if (n3.isElement() && (n3.toElement().tagName() == QString("vertex"))) {
	QDomElement vertexElement = n3.toElement();
	
	if (lastvertex<3) {
	  triangleopenglarray.vertexarray[lastvertex].x = atof(vertexElement.attribute("x").latin1());
	  triangleopenglarray.vertexarray[lastvertex].y = atof(vertexElement.attribute("y").latin1());
	  triangleopenglarray.vertexarray[lastvertex].z = atof(vertexElement.attribute("z").latin1());
	  lastvertex++;
	}
      }
      else {
	/* get 3 texcoords */
	if (n3.isElement() && (n3.toElement().tagName() == QString("texcoord"))) {
	  QDomElement texcoordElement = n3.toElement();
	  
	  if (lasttexturecoord<3) {
	    triangleopenglarray.texcoordarray[lasttexturecoord].x = atof(texcoordElement.attribute("x").latin1());
	    triangleopenglarray.texcoordarray[lasttexturecoord].y = atof(texcoordElement.attribute("y").latin1());
	    lasttexturecoord++;
	  }
	}
	else {
	  /* allow to add arbitrary objects */
	  DrawSceneObject *object = scene->parseObject(n3);
	  if (object) {
	    addChild(object);
	  }
	}
      }
    }

    n3 = n3.nextSibling();
  }
}

void DrawSceneObjectTriangle::compile(std::vector<openglarray *> &targetlist, pbr_list *pbrlist) {
  /* compile this triangle */
  DrawSceneObjectTexture *texture = (DrawSceneObjectTexture *) scene->getRegisteredDrawSceneObject(texturename);

  triangleopenglarray.texture = texture;
  triangleopenglarray.vertexcount = 3;

  /* compile all children */
  DrawSceneObject::compile(targetlist, pbrlist);

  /* merge this triangle to the list */
  mergeArray(targetlist, &triangleopenglarray);
}

