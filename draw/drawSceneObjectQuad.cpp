#include "drawSceneObjectQuad.h"

DrawSceneObjectQuad::DrawSceneObjectQuad(DrawScene *scene) 
  : DrawSceneObject(scene) {

  for(int i=0; i<2; i++) {
    triangleopenglarray[i].vertexcount = 3;
    triangleopenglarray[i].vertexarray = new Point3D[3];
    triangleopenglarray[i].texcoordarray = new Point2D[3];
  }
}

DrawSceneObjectQuad::~DrawSceneObjectQuad() {
  for(int i=0; i<2; i++) {
    delete(triangleopenglarray[i].vertexarray);
    delete(triangleopenglarray[i].texcoordarray);
  }
}

void DrawSceneObjectQuad::readXMLData(QDomNode n) {
  registerXMLNames(n);

  int lastvertex = 0, lasttexturecoord = 0;

  /* n contains the triangle node */
  QDomNode n3 = n.firstChild();
  while( !n3.isNull() ) {
    /* get a texture */
    if (n3.isElement() && (n3.toElement().tagName() == QString("usetex"))) {
      texturename = n3.toElement().attribute("name");
    }
    else {
      /* get 4 vertices */
      if (n3.isElement() && (n3.toElement().tagName() == QString("vertex"))) {
	QDomElement vertexElement = n3.toElement();
	
	if (lastvertex<4) {
	  vertexarray[lastvertex].x = atof(vertexElement.attribute("x").latin1());
	  vertexarray[lastvertex].y = atof(vertexElement.attribute("y").latin1());
	  vertexarray[lastvertex].z = atof(vertexElement.attribute("z").latin1());
	  lastvertex++;
	}
      }
      else {
	/* get 3 texcoords */
	if (n3.isElement() && (n3.toElement().tagName() == QString("texcoord"))) {
	  QDomElement texcoordElement = n3.toElement();
	  
	  if (lasttexturecoord<4) {
	    texcoordarray[lasttexturecoord].x = atof(texcoordElement.attribute("x").latin1());
	    texcoordarray[lasttexturecoord].y = atof(texcoordElement.attribute("y").latin1());
	    lasttexturecoord++;
	  }
	}
	else {
	  /* allow to add arbitrary objects */
	  DrawSceneObject *object = scene->parseObject(n3);
	  if (object) {
	    addChild(object);
	  }
	}
      }
    }

    n3 = n3.nextSibling();
  }
}

void DrawSceneObjectQuad::compile(std::vector<openglarray *> &targetlist, pbr_list *pbrlist) {
  /* compile this triangle */
  DrawSceneObjectTexture *texture = (DrawSceneObjectTexture *) scene->getRegisteredDrawSceneObject(texturename);

  triangleopenglarray[0].texture = texture;
  triangleopenglarray[0].vertexcount = 3;
  triangleopenglarray[1].texture = texture;
  triangleopenglarray[1].vertexcount = 3;

  for(int v=0; v<3; v++) {
    triangleopenglarray[0].vertexarray[v] = vertexarray[v];
    triangleopenglarray[0].texcoordarray[v] = texcoordarray[v];

    triangleopenglarray[1].vertexarray[v] = vertexarray[(v+2)%4];
    triangleopenglarray[1].texcoordarray[v] = texcoordarray[(v+2)%4];
  }

  /* compile all children */
  DrawSceneObject::compile(targetlist, pbrlist);

  /* merge this triangle to the list */
  mergeArray(targetlist, &triangleopenglarray[0]);
  mergeArray(targetlist, &triangleopenglarray[1]);
}

