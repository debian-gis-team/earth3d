#include "graphicsObjectsContainer.h"
#include <stdio.h>
#include "globalsettings.h"
#include "formview.h"
#include "insertListDrawItemEvent.h"

GraphicsObjectsContainer::GraphicsObjectsContainer(FormView *formview) {
  this->formview = formview;
}

GraphicsObjectsContainer::~GraphicsObjectsContainer() {
  printf("~GraphicsObjectsContainer\n");

  QMutexLocker qml(&objectListMutex);
  vector<Draw *>::iterator i = objectList.begin();
  while(i!=objectList.end()) {
    delete(*i);
    i++;
  }
}

void GraphicsObjectsContainer::add(Draw *td, QString *identifier) {
  QMutexLocker qml(&objectListMutex);
  objectList.push_back(td);

  needRedraw = true;
  qApp->postEvent(formview, new InsertListDrawItemEvent(td));
//   formview->addToDrawList(td);

  if (identifier) {
    gocTypes[*identifier]=td;
  }
}

void GraphicsObjectsContainer::remove(Draw *td) {
  QMutexLocker qml(&objectListMutex);
  vector<Draw *>::iterator i = objectList.begin();
  while(i!=objectList.end()) {
    if ((*i)==td) {
      objectList.erase(i);
      break;
    }
    i++;
  }

  /* remove from gocTypes list */
  QMap<QString, Draw *>::iterator ii;
  for(ii = gocTypes.begin(); ii != gocTypes.end(); ii++) {
    if (*ii==td) {
      gocTypes.erase(ii);
      break;
    }
  }

  /* remove from GUI */
  formview->removeFromDrawList(td);
  needRedraw = true;
}

vector<Draw *> *GraphicsObjectsContainer::getList() {
  return(&objectList);
}

Draw *GraphicsObjectsContainer::getListElement(int nr) {
  QMutexLocker qml(&objectListMutex);
  return(objectList[nr]);
}

int GraphicsObjectsContainer::getListSize() {
  QMutexLocker qml(&objectListMutex);
  return(objectList.size());
}

Draw *GraphicsObjectsContainer::findListElementByIdentifier(QString identifier) {
  return(gocTypes[identifier]);
}
