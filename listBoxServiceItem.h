#ifndef LISTBOXSERVICEITEM_H
#define LISTBOXSERVICEITEM_H
#include <qdom.h>
#include <qlistbox.h>
class ListBoxServiceItem : public QListBoxText {
  char *connection;
  char *sender;

 public:
  ListBoxServiceItem ( QDomNode text, const char *connection, const char *sender );
  ~ListBoxServiceItem ();

  char *getConnection();
  char *getSender();
};

#endif
